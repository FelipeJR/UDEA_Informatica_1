palabra = input("Ingrese una palabra: ")
alfabeto = "abcdefghijklmnopqrstuvwxyz"
palabramod = ""
n = -1

while not (0 <= n <= 26):
    n = int(input("Ingrese un número no mayor a 26: "))
    
    if not (0 <= n <= 26):
        print("Ingrese un número válido.")

i = 0

while i < len(palabra):
    letra = palabra[i]
    j = 0
    encontrado = False
    while j < len(alfabeto) and encontrado == False:
        if alfabeto[j] == letra:
            encontrado = True
        else:
            j += 1
    
    if encontrado == False:
        palabramod += letra
    else:
        newpos = (j+n) % 26
        palabramod += alfabeto[newpos]
    
    i += 1

print("La palabra cifrada es:", palabramod)
