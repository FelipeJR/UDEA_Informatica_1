"""
Felipe Jiménez Ramírez
Juan Esteban López Castrillón
Grupo 4

Teniendo en cuenta que un carro necesita cambio de aceite cada 5.678 km, haga un
algoritmo que calcule cuántos cambios de aceite ha tenido un carro según el total de
kilómetros que ha recorrido.
"""
km = int(input("Ingrese los kilometros recorridos: ")) #Se le solicita al usuario que ingrese el valor de los kilometros recorridos
cal_cambios = (km//5678) #cambios: cálcula el número de cambios con una división entera entre el valor ingresado por el usuario(km) y 5678
print(cal_cambios, "cambios de aceite ") #Se imprime en pantalla el número de cambios de aceite 