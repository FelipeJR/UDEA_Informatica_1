"""
Felipe Jiménez Ramírez
Juan Esteban López Castrillón
Grupo 4

Haga un algoritmo que muestre el siguiente patrón en la pantalla:
**************
*****    *****
***        ***
*            *

El tamaño del patrón estará determinado por un número entero impar que ingrese el
usuario. En el ejemplo mostrado, el tamaño de la figura es 7. 
"""

num = int(input("Ingrese un número impar: "))

numLineas = (num//2)
cont_esp = 0
i = 0

while i <= numLineas:
    linea = ""
    j = 1
    while j<=num:
        linea += "+"
        j += 1
    espacios = ""
    j = 1
    while j<= cont_esp:
        espacios += " "
        j += 1
    
    print(linea+espacios+linea)
    cont_esp +=4
    num -= 2
    i +=1

          
