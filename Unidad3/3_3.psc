Algoritmo palindroma
	Escribir 'Ingrese una palabra o frase: '
	Leer palabra
	palabra <- minusculas(palabra)
	palabra_sin_espacios <- ''
	indice <- 0
	Mientras indice<longitud(palabra) Hacer
		letra <- subcadena(palabra,indice,indice)
		Si letra<>' ' Entonces
			palabra_sin_espacios <- palabra_sin_espacios+letra
		FinSi
		indice <- indice+1
	FinMientras
	i <- longitud(palabra_sin_espacios)-1
	palabra_invertida <- ''
	Mientras i>=0 Hacer
		palabra_invertida <- palabra_invertida+subcadena(palabra_sin_espacios,i,i)
		i <- i-1
	FinMientras
	Si palabra_sin_espacios == palabra_invertida Entonces
		Escribir palabra, " es palindroma"
	SiNo
		Escribir palabra, " no es palindroma"
	FinSi
FinAlgoritmo
