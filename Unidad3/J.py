"""
Felipe Jiménez Ramírez
Juan Esteban López Castrillón
Grupo 4

Enunciado:
Calcular el máximo común divisor (MCD) de 2 enteros. Por ejemplo, si los números
son 12 y 36, el resultado es 12; si son 34 y 85 el resultado es 17

"""

num1 = int(input("Ingrese el número 1: "))
num2 = int(input("Ingrese el número 2: "))

if num2 > num1:
    aux = num1
    num1 = num2
    num2 = aux

while num2 !=0:
    residuo = num1 % num2
    num1 = num2
    num2 = residuo

print("El MCD de los números ingresados es: ", num1)