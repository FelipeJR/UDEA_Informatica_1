palabra = input("Ingrese una palabra o frase: ").lower()
indice = 0
palabra_sin_espacios = ""

while indice < len(palabra):
    letra = palabra[indice]
    if letra != " ":
        palabra_sin_espacios += letra
    indice += 1


# Invertir la palabra
i = len(palabra_sin_espacios) - 1
palabra_invertida = ""
while i >= 0:
    palabra_invertida += palabra_sin_espacios[i]
    i -= 1

# Verificar si es palíndromo
if palabra_sin_espacios == palabra_invertida:
    print(palabra_invertida, "es palindroma")
else:
    print(palabra_invertida, "no es palindroma")
