"""
Felipe Jiménez Ramírez
Juan Esteban López Castrillón
Grupo 4

Enunciado:
Dado el valor de los lados de un triángulo Isósceles, haga un algoritmo que calcule
su perímetro, su altura y su área.

Entradas:
l1: Almacena el valor del lado que se repite y lo almacena en la variable l1
l2: Almacena el valor del lado que no se repite y lo almacena en la variable l1

Salidas: 
perimetro: Se cálcula el perimetro del tríangulo
altura: Se imprime el valor de altura
area: Se imprime el valor de area

Auxiliares:

"""

l1 = float(input("Ingrese el lado que se repite: ")) 
l2 = float(input("Ingrese el lado que no se repite: ")) #solicita al usuario el valor del lado que no se repite y lo almacena en la variable l2
perimetro = (l1*2)+l2 
altura = ((l1**2)-(l2/2)**2)**(1/2) #Se cálcula la altura del triángulo
area = (l2*altura)/2 #Se cálcula el area del tríangulo

print("El perimetro del triángulo ingresado es: ", perimetro) #Se imprime el valor de perimetro
print("La altura del triángulo ingresado es: ", altura)
print("El área del triángulo ingresado es: ", area)