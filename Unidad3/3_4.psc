Algoritmo cifrado
	escribir "Ingrese una palabra:"
	leer palabra
	escribir "Ingrese un número: "
	leer n
	alfabeto <- "abcdefghijklmnopqrstuvwxyz"
	palabramod <- ""
	
	i <- 0
	mientras i < Longitud(palabra)
		letra <- Subcadena(palabra, i, i)
		j <- 0
		encontrado <- Falso
		mientras j < Longitud(alfabeto) y encontrado == Falso
			si Subcadena(alfabeto, j, j) == letra
				encontrado <- Verdadero
			SiNo
				j <- j + 1
			FinSi
		FinMientras
		si encontrado == Falso
			palabramod <- palabramod + letra
		SiNo
			newpos <- (j + n) MOD 26
			palabramod <- palabramod + Subcadena(alfabeto, newpos, newpos)
		FinSi
		i <- i + 1
	FinMientras
	
	escribir palabramod

FinAlgoritmo
