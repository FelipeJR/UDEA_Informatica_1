Algoritmo conversion
	// Felipe Jiménez Ramírez
	// Juan Esteban López Castrillón
	// Grupo 4
	letra <- '' // letra: se inicializa el proceso letra en un caracter vacío
	Escribir 'Ingrese una letra: ' // Se le solicita al usuario que ingrese una letra
	Leer letra // Almacena el valor de la letra ingresada por el usuario pero este se inicializa previamente como un caracter vacío
	letra <- Minusculas(letra) // El proceso convierte el caracter ingresado en una minusculas
	Si letra=='a' O letra=='e' O letra=='i' O letra=='o' O letra=='u' Entonces
		Escribir 'Es una vocal'
	SiNo
		Si letra=='b' O letra=='c' O letra=='d' O letra=='f' O letra=='g' O letra=='h' O letra=='j' O letra=='k' O letra=='l' O letra=='m' O letra=='n' O letra=='p' O letra=='q' O letra=='r' O letra=='t' O letra=='v' O letra=='w' O letra=='x' O letra=='y' O letra=='z' Entonces
			Escribir 'Es consonante'
		FinSi
	FinSi
FinAlgoritmo

