"""
Felipe Jiménez Ramírez
Juan Esteban López Castrillón
Grupo 4

Escriba un programa que le pida al usuario una palabra o frase y una letra. El programa deberá imprimir
la misma frase o palabra ingresada, pero ocultando la letra que ingresó el usuario con un asterisco.


Entradas:
palabra: Almacena la palabra ingresada por el usuario
letra: Almacena el caracter ingresado por el usuario que será remplazado 

Salidas: 
nueva_palabra: se imprimirá la palabra con la letra ingresada por el usuario remplazada por el *

Auxiliares:
palabra: almacena la palabra que será usada 
letra: almacena la letra que será cambiada por un asterisco
astAux: almacena el asterisco como caracter 
"""

palabra = input("Ingrese una palabra: ")
letra = input("Ingrese una letra: ")
nueva_palabra = ""
astAux = "*"

i = 0
while i < len(palabra):
    caracter = palabra[i]
    i += 1
    if caracter != letra:
        nueva_palabra += caracter
    else:
        nueva_palabra += astAux

print(nueva_palabra)

