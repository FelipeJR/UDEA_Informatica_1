"""
Felipe Jiménez Ramírez
Juan Esteban López Castrillón
Grupo 4

Enunciado:
Haga un algoritmo para calcular el costo de un servicio de video streaming por
demanda. El algoritmo recibirá el momento en que inició y terminó la reproducción de
videos, mediante dos números enteros de máximo 6 dígitos que representan las horas
(00-23), los minutos (00-59) y los segundos (00-59). El costo del servicio es de $2 pesos
por segundo, con un cobro mínimo de $1.000 pesos. Por ejemplo, si los tiempos de
inicio y fin son 23754 y 130231, el costo del servicio será de $74.942 pesos. 

"""

horaInicio = int(input("Ingrese la hora de inicio en formato HHMMSS: "))
horaFinal = int(input("Ingrese la hora de finalización en formato HHMMSS: "))

SegundosTotales1 = ((horaInicio//10000)*3600)+(((horaInicio//100)%100)*60)+(horaInicio%100)
SegundosTotales2 = ((horaFinal//10000)*3600)+(((horaFinal//100)%100)*60)+(horaFinal%100)
diferencia = SegundosTotales2-SegundosTotales1
costoTotal = diferencia * 2

if diferencia < 0:
    diferencia = diferencia * -1

print("El costo total es:", costoTotal)



