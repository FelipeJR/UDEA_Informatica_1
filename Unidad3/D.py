"""
Felipe Jimènez Ramírez
Juan Esteban López Castrillòn
Grupo 4

"""

horas = int(input("Ingrese el numero de horas : "))

minutos = int(input("Ingrese el numero de minutos : "))

totalTiempo = (horas * 60 ) + minutos

precio_normal = totalTiempo * 7

incremento  = totalTiempo * 50

cobroFijo = totalTiempo * 96000

if (totalTiempo<=100):
    print("Usted uso la bicicleta por " , totalTiempo , " minutos y debe pagar : " , precio_normal)

elif (totalTiempo > 100 and totalTiempo < 1440):
    print("Usted uso la bicicleta por " , totalTiempo , "minutos y debe pagar : " , incremento)

elif (totalTiempo >= 1440):
    print("Usted uso la bicicleta por " , totalTiempo , " minutos y debe pagar : " , cobroFijo)
