"""
Felipe Jiménez Ramírez
Juan Esteban López Castrillón
Grupo 4

Enunciado:
Escriba un programa que tome un carácter (es decir, un string de longitud 1) y determine si el carácter es
vocal o consonante.

Entradas:
letra: Almacena el valor de la letra ingresada por el usuario pero este se inicializa previamente como un caracter vacío

Salidas: 
Es vocal o No es vocal
"La salida se define con un condicional"

Auxiliares:
VocalMinus:  almacena las vocales y es usada para la comparación
consonantes: almacena las consonantes y es usada para la comparación

"""
letra = ""
vocalesMinus = "aeiou"
consonantes = "bcdfghjklmnpqrstvwxyz"
letra = input("Ingrese una letra: ").lower()

if letra in vocalesMinus:
    print("Es una vocal")
elif letra in consonantes :
    print("Es una consonante")