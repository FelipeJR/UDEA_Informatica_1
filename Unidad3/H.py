"""
Felipe Jimènez Ramírez
Juan Esteban López Castrillòn
Grupo 4

"""

devuelta = int(input("Ingrese la devuelta : ")) #Se le solicita que se ingrese la devuelta

dineroRestante = devuelta #dinero_restante: inicialmente toma el valor de devuelta, pero este cambiara durante la ejecucion

monedas_1000 = dineroRestante//1000 #monedas_1000: Se calcula por medio de una division entera la cantidad de monedas de 1000 con ayuda del valor que tiene dinero_restante

dineroRestante = dineroRestante % 1000 #Se calcula el dinero restante por medio del módulo

monedas_500 = dineroRestante//500  #monedas_500: Se calcula por medio de una division entera la cantidad de monedas de 500 con ayuda del valor que tiene dinero_restante

dineroRestante = dineroRestante % 500 #Se calcula el dinero restante por medio del módulo

monedas_200 = dineroRestante//200 #monedas_200: Se calcula por medio de una division entera la cantidad de monedas de 200 con ayuda del valor que tiene dinero_restante

dineroRestante <- dineroRestante % 200 #Se calcula el dinero restante por medio del módulo

monedas_100 = dineroRestante //100 #monedas_100: Se calcula por medio de una division entera la cantidad de monedas de 100 con ayuda del valor que tiene dinero_restante

dineroRestante <- dineroRestante % 100 #Se calcula el dinero restante por medio del módulo

monedas_50 = dineroRestante // 50 #monedas_50: Se calcula por medio de una division entera la cantidad de monedas de 50 con ayuda del valor que tiene dinero_restante

dineroRestante = dineroRestante % 50 #Se calcula el dinero restante por medio del módulo

if (dineroRestante > 0 ):
    print('Se deben devolver ',monedas_1000,' de mil, ',monedas_500,' de quinientos, ',monedas_200,' de docientos, ',monedas_100,' de cien, ',monedas_50,' de cincuenta,',' y quedan por entregar ',dineroRestante )#Se imprime la cantidad de monedas de cada tipo incluyendo el dinero restante
else:
    print('Se deben devolver ',monedas_1000,' de mil, ',monedas_500,' de quinientos, ',monedas_200,' de docientos, ',monedas_100,' de cien, y ',monedas_50,' de cincuenta') #Si no hay dinero restante se imprime la cantidad de monedas de cada tipo