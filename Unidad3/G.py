"""
Felipe Jiménez Ramírez
Juan Esteban López Castrillón
Grupo 4

Haga un algoritmo que muestre en pantalla las soluciones de la ecuación
ax2+bx+c=0, dados valores para los coeficientes a, b y c. 
Forma de la ecuación cuadrático: ax^2+bx+c
"""
a = int(input("Ingrese el coeficiente cuarático: ")) #Se le solicita al usuario que ingrese el coeficiente cuadrático
b = int(input("Ingrese el coeficiente líneal: ")) #Se le solicita al usuario que ingrese el coeficiente líneal
c = int(input("Ingrese el termino independiente: ")) #Se le solicita al usuario que ingrese el termino independiente

discriminante= (((b**2)-(4*a*c))**(1/2)) #discriminante: se sustituyen los valores y se calcula el valor de la discriminante
raiz1 = ((-b)+discriminante)/(2*a) #raiz1: Encuentra el valor de la raiz 1
raiz2 = ((-b)-discriminante)/(2*a) #raiz2: Encuentra el valor de la raiz 2

if isinstance(discriminante, complex):
    print("Serán dos raices complejas conjugadas")
    print('X1: '+ str(raiz1) + " X2: "+ str(raiz2)) #Se imprime la raiz1 y la raiz2

elif discriminante == 0:
    print("El resultado serán 2 raices reales y repetidas") #Se imprime el tipo de solución
    print('X1: '+ str(raiz1) + " X2: "+ str(raiz2)) #Se imprime la raiz1 y la raiz2
elif discriminante > 0:
    print("El resultado serán 2 raices reales y diferentes") #Se imprime el tipo de solución
    print('X1: '+ str(raiz1) + " X2: "+ str(raiz2)) #Se imprime la raiz1 y la raiz2

