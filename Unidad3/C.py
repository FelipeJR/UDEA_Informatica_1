"""
Felipe Jiménez Ramírez
Juan Esteban López Castrillón
Grupo 4

Determinar la cantidad de material necesario para cubrir una caneca cilíndrica, con
tapa, dado su radio y altura. ¿Cuánto líquido es capaz de almacenar?
"""
PI = 3.1415
radio = int(input("Ingrese el valor del radio del cilindro: ")) #Almacena en la variable radio el valor ingresado por el usuario
altura = int(input("Ingrese el valor de la altura del cilindro a calcular: ")) #Se solicita al usuario que ingrese la altura del cilindro

vol = PI*(radio**2)*altura  #Se calcula el volumen del cilindro y se almacena en la variable vol
area_s = ((2*PI*radio)*(radio+altura)) #Se calcula el área superficial del cilindro y se almacena en la variable area_s

print("El cilindro es capaz de almacenar: " + str(vol)) 
print("La cantidad de material necesario para cubrir la caneca cilindrica es: " + str(area_s))


