"""
Felipe Jiménez Ramírez
Juan Esteban López Castrillón
Grupo 4

Haga un algoritmo que muestre el siguiente patrón en la pantalla:
*
***
*****
*******
*****
***
*
El tamaño del patrón estará determinado por un número entero impar que ingrese el
usuario. En el ejemplo mostrado, el tamaño de la figura es 7. 
"""

n = int(input("Ingrese el tamaño del triángulo: "))
i = 1

while  i <= n:
    j=1
    while j<=i:
        print("*", end = "")
        j += 1
    print("")
    if i == n:
        n -= 1
        while n >= 1:
            s=1
            while s<=n-1:
                print("*", end = "")
                s+=1
            print("")
            n-=2
    i += 2

