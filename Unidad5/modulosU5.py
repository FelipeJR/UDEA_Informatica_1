"""
Felipe Jiménez Ramírez
Juan Esteban López Castrillón
Grupo 4

"""

#------------------------------------------ Modulos Generales del Sistema ------------------------------------------#

def valorMin(lista):

    """
    Sinopsis:
        Función permite encontrar el valor minimo en una lista de números


    Entradas y salidas:
        lista: lista de números
        minimo: valor minimo encontrado

    """

    minimo = lista[0]
    for i in lista:
        if i < minimo:
            minimo = i
    return minimo


def valorMax(lista):

    """
    Sinopsis:
        Función permite encontrar el valor maximo en una lista de números


    Entradas y salidas:
        lista: lista de números
        máximo: valor mayor encontrado

    """

    maximo = lista[0]
    for i in lista:
        if i > maximo:
            maximo = i
    return maximo

def sumList(lista):

    """
    Sinopsis:
        Función permite calcular la suma de todos los elementos de una lista


    Entradas y salidas:
        - lista: lista base que contiene los datos
        - returns: suma

    """

    suma = 0
    for i in lista:
        suma += i
    return suma

def obtener_fecha():

    """
    Sinopsis:
        Función que permite obtener la fecha exacta del dispositivo


    Entradas y salidas:
        - returns: ahora, fecha del equipo
    """

    from datetime import datetime
    ahora = str(datetime.now())
    return ahora[0:10]


def limpiar_pantalla():

    """
    Sinopsis:
        Función que permite simular un limpiado de pantalla


    Entradas y salidas:
        - returns: none, solo imprime 20 saltos de línea
    """

    print('\n'*20)


def validar_fecha(fecha):

    """
    Sinopsis:
        Función que permite validar si una fecha está en el formato dado


    Entradas y salidas:
        - fecha: fecha que será validada
        - returns: True o False dependiendo de si es valida
    """

    if fecha[4] != "-" or \
    fecha[7] != "-" or \
    fecha[0] != "2" or \
    len(fecha) != 10 or \
    int(fecha[5:7]) < 1 or int(fecha[5:7]) > 12 or \
    int(fecha[8:10]) < 1 or int(fecha[8:10]) > 31:

        return False
    else:
        return True

def validacion_rango(valor, minimo, maximo):

    """
    Sinopsis:
        Función que permite validar si un número está en un rango dado


    Entradas y salidas:
        - valor: valor para hacer la comprobación
        - minimo: valor minimo del rango
        - maximo: valor maximo del rango
        - returns: True o False dependiendo de si es valido o no
    """

    if valor >= minimo and valor <= maximo or valor == "ND":
        return True
    else:
        return False

def open_file(archivo):

    """
    Sinopsis:
        funcion que permite abrir un archivo dado como argumento y cargarlo en memoria

    Entradas y salidas:
        - archivo: nombre del archivo
        - returns: lineas 
    """

    with open(archivo) as file:
        lineas = file.readlines()
        return lineas
    
def posLineWhite(archivo):

    """
    Sinopsis:
        funcion que permite calcular la posición de las lineas que está completamente en blanco de un archivo

    Entradas y salidas:
        - archivo: nombre del archivo
        - returns: posLineWhite 
    """

    lectura = open_file(archivo) 
    posLineWhite = []
    pos = 0

    for linea in lectura:
        linea_limpia = linea.rstrip()
        if len(linea_limpia) == 0:
            posLineWhite.append(pos)
        pos += 1

    return posLineWhite

def search_position(linea, separadores):

    """
    Sinopsis:
        funcion que nos permite encontrar las ubicación exacta de los separadores en el string ingresado

    Entradas y salidas:
        - linea: string a evaluar
        - separadores: delimitador a buscar
        - returns: pos: posiciones
    """

    pos = []
    posi = 0
    for caracter in linea:
        posi += 1
        if caracter in separadores:
            pos.append(posi-1)
    return pos

def custom_split(string,separadores):
    
    """
    Sinopsis:
        funcion que nos permite encontrar las ubicación exacta de los separadores en el string ingresado

    Entradas y salidas:
        - linea: string a evaluar
        - separadores: delimitador a buscar
        - returns: pos: posiciones

    """

    pos = search_position(string, separadores)
    list_elementos = []

    # Verificar si el primer elemento no tiene separador antes de él
    if pos[0] != 0:
        list_elementos.append(string[:pos[0]])
        
    # Iterar por los separadores encontrados para obtener los elementos
    for i in range(len(pos) - 1):
        indice0 = pos[i] + 1
        indice1 = pos[i+1]
        list_elementos.append(string[indice0:indice1])
    # Agregar el último elemento
    indice0 = pos[-1] + 1
    list_elementos.append(string[indice0:])
    return list_elementos

def respuesta_numerica(string,digitos_validos_str):

    """
    Sinopsis:
        Función que permite validar si el usuario ingresa un número de un 
        conjunto especifico dado como argumento


    Entradas y salidas:
        - digitos_validos_str: conjunto de números validos en str
        - string: texto que se mostrará por pantalla al ejecutar el input
        - returns: num
    """

    num = ""
    while num not in digitos_validos_str or len(num) != 1 or len(num) != 1:
        num = input(string)
        if num not in digitos_validos_str or len(num) != 1:
            print("La opción no es valida")
    return num

def validar_existencia(documento,diccionario):

    """
    Sinopsis:
        Función que comprueba la existencia de una clave en un diccionario 

    Entradas y salidas:
        - documento: documento que se buscará en la base de datos
        - diccionario: diciconario donde se buscará el documento 
        - returns: num
    """

    for clave in diccionario:
        if documento in clave:
            return True
    return False

def generar_codigo(diccionario):

    """
    Sinopsis:
        funcion que usa el diccionarion de estaciones, itera entre todos las claves que 
        corresponden a los códigos y retorna un número mayor que el código mayor del diccionario.


    Entradas y salidas:
        - diccionario: diccionario de estaciones
        - returns: nuevo código compatible con los requerimientos
    """

    if len(diccionario) == 0:
        return "1"
    
    mayor = 0
    for clave in diccionario:
        if int(clave) > mayor:    
            mayor = int(clave)
        cod = mayor + 1
    return str(cod)

def imprimir_tabla(tabla, ancho, encabezado=None):  
    ''' 
    Imprime en pantalla un tabla con los datos pasados, ajustado a los tamaños deseados.
    
    Argumentos:
        tabla: Lista que representa la tabla. Cada elemento es una fila
        ancho: Lista con el tamaño deseado para cada columna. Si se especifica
            un entero, todas las columnas quedan de ese tamaño
        encabezado: Lista con el encabezado de la tabla
    '''
    def dividir_fila(ancho,sep='-'):
        '''
        ancho: Lista con el tamaño de cada columna
        se: Caracter con el que se van a formar las líneas que 
            separan las filas
        '''
        linea = ''
        for i in range(len(ancho)):
            linea += ('+'+sep*(ancho[i]-1))
        linea = linea[:-1]+'+'
        print(linea)
        
    def imprimir_celda(texto, impresos, relleno):
        '''
        texto: Texto que se va a colocar en la celda
        impresos: cantidad de caracteres ya impresos del texto
        relleno: cantidad de caracteres que se agregan automáticamente,
            para separar los textos del borde de las celda.
        '''        
        # Imprimir celda
        if type(texto) == type(0.0):
            #print(texto)
            texto = '{:^7.2f}'.format(texto)
            #print(type(texto), texto)
        else:
            texto = str(texto)
        texto = texto.replace('\n',' ').replace('\t',' ')
        if impresos+relleno < len(texto):
            print(texto[impresos:impresos+relleno],end='')
            impresos+=relleno
        elif impresos >= len(texto):
            print(' '*(relleno),end='')
        else:
            print(texto[impresos:], end='')
            print(' '*(relleno-(len(texto) - impresos)),end='')
            impresos = len(texto)
        return impresos
    
    def imprimir_fila(fila):
        '''
        fila: Lista con los textos de las celdas de la fila
        '''
        impresos = []   
        alto = 1
        for i in range(len(fila)):
            impresos.append(0)
            if type(fila[i]) == type(0.0):
                texto = '{:7.2f}'.format(fila[i])
            else:
                texto = str(fila[i])
            alto1 = len(texto)//(ancho[i]-4)
            if len(texto)%(ancho[i]-4) != 0:
                alto1+=1
            if alto1 > alto:
                alto = alto1
        for i in range(alto):
            print('| ',end='')
            for j in range(len(fila)):
                relleno = ancho[j]-3
                if j == len(fila)-1:
                    relleno = ancho[j] -4
                    impresos[j] = imprimir_celda(fila[j], impresos[j], relleno)
                    print(' |')
                else:
                    impresos[j] = imprimir_celda(fila[j], impresos[j], relleno)
                    print(' | ',end='')   
    if not len(tabla) > 0:
        return
    if not type(tabla[0]) is list:
        return
    ncols = len(tabla[0])
    if type(ancho) == type(0):
        ancho = [ancho+3]*ncols 
    elif type(ancho) is list:
        for i in range(len(ancho)):
            ancho[i]+=3
    else:
        print('Error. El ancho debe ser un entero o una lista de enteros')
        return
    assert len(ancho) == ncols, 'La cantidad de columnas no coincide con los tamaños dados'
    ancho[-1] += 1
    if encabezado != None:
        dividir_fila(ancho,'=')
        imprimir_fila(encabezado)
        dividir_fila(ancho,'=')
    else:
        dividir_fila(ancho)
    for fila in tabla:
        imprimir_fila(fila)
        dividir_fila(ancho)

#------------------------------- Modulos Creación, Gestión y Eliminación de Usuarios -------------------------------#
            
def validar_identificacion(documento):

    """
    Sinopsis:
        Función que permite validar si un documento de identidad tiene únicamente 10 digitos.


    Entradas y salidas:
        - documento: documento de identidad en str
        - returns: True o False dependiendo de si es valido
    """

    contDigitos = 0
    digitos = "0123456789"

    while contDigitos != 10:
        #contDigitos = 0
        for d in documento:
            if d in digitos:
                contDigitos += 1
            
            if contDigitos != 10 and len(documento) != 10 or d not in digitos:
                return False
    return True

def identificacion():
    
    """
    Sinopsis:
        Función que le solicita al usuario que ingrese un documento de identidad y valida si cumple 
        con los requerimientos.


    Entradas y salidas:
        - None
        - returns: documento 
    """

    documento = " "
    while validar_identificacion(documento) != True:
        documento = input("Ingrese documento de identidad del usuario: ")
        if validar_identificacion(documento) != True:
            print("Ingrese un documento valido")

    return documento 

def validar_nombre_usuario(nombre):
    
    """
    Sinopsis:
        Función que permite validar si un nombre de usuario es valido, es decir que solo tenga 
        letras y espacios.


    Entradas y salidas:
        - nombre: nombre de usuario ingresado por el usuario
        - returns: True o False dependiendo de si es valido
    """

    nombre = nombre.lower()
    cont = 1
    cInvalidos = "0123456789!@#\$%\^&\*\(\)_\+-=\[\]{}\\\|;':\",.<>\?\/"
    while cont != 0:
        for caracter in nombre:
            if caracter in cInvalidos:
                return False
        return True
    
def nombre_usuario():

    """
    Sinopsis:
        Función que le solicita al usuario el nombre de usuario para la plataforma y valida 
        si es valido, es decir que solo tenga letras y espacios, por otra parte convierte las
        mayusculas a minusculas.


    Entradas y salidas:
        - None
        - returns: nombre_usuario
    """

    nombreU = "1"
    while validar_nombre_usuario(nombreU) != True:
        nombreU = input("Ingrese el nombre de usuario: ").lower()
        if validar_nombre_usuario(nombreU) != True:
            print("El nombre de usuario solo puede contener letras y espacios, intenta nuevamente.")
    return nombreU 

def validar_password(password):

    """
    Sinopsis:
        Función que permite validar si una contraseña de usuario es valida, es decir que tenga
        minimo 4 digitos. 


    Entradas y salidas:
        - password: contraseña ingresada como argumento
        - returns: True o False dependiendo de si es valida
    """

    longitud = len(password)
    if longitud < 4:
        return False
    else:
        return True
    
def password(modo):

    """
    Sinopsis:
        Funcion que le solicita al usuario que ingrese una contraseña, verifica 
        si es valida y la retorna; por otra parte tiene dos modos:
        1. Para escribir la contraseña una sola vez
        2. Para escribir la contraseña y confirmarla


    Entradas y salidas:
        - modo: 1 o 2 dependiendo de lo que se requiera
        - returns: passwordA
    """

    passwordA = ""

    if modo == 1:
        while validar_password(passwordA) != True:
            passwordA = input("Ingrese la contraseña: ")
            if validar_password(passwordA) != True:
                print("Ingrese una contraseña de minimo 4 caracteres")

    elif modo == 2:
        comprobacion = False
        while validar_password(passwordA) != True or comprobacion != True:
            passwordA = input("Ingrese la contraseña: ")
            if validar_password(passwordA) != True:
                print("Ingrese una contraseña de minimo 4 caracteres")
            else:
                passwordB = input("Confirme su contraseña: ")
                if passwordA == passwordB:
                    comprobacion = True
                else:
                    print("Las credenciales son diferentes, ingrese la contraseña y confirme nuevamente")
    return passwordA

def crear_usuario(diccionario):

    """
    Sinopsis:
        Función que hace uso de otras funciones creadas anteriormente y recopila el documento de 
        identificación, contraseña y rol para después retornar un diccionario agregando esta 
        información.


    Entradas y salidas:
        - diccionario: diccionario que se usará como base para consultar la existencia de documentos y 
        se actualizará con los nuevos usuarios. 
        - returns: none solo se hacen las ediciones correspondientes
    """

    ID = list(diccionario.keys())[0]
    while validar_existencia(ID, diccionario):
        ID = identificacion()
        if validar_existencia(ID, diccionario):
            print("Este documento de identidad ya está registrado en la base de datos")

    user = nombre_usuario()
    contrasena = password(2)
    rol = respuesta_numerica("Seleccione el rol 1. Admin y 2. Operador:","12")

    if rol == "2":
        diccionario[ID] = {'usuario': user, 'contrasena': contrasena, 'rol': 'Operador'}
    elif rol == "1":
        diccionario[ID] = {'usuario': user, 'contrasena': contrasena, 'rol': 'Administrador'}

def editar_usuario(diccionario):

    """
    Sinopsis:
        Función que hace uso de otras funciones creadas anteriormente, permite editar usuarios existentes
        en un diccionario


    Entradas y salidas:
        - diccionario: diccionario que se usará como base para consultar la existencia de documentos y 
        edición de usuarios 
        - returns: none solo se hacen las ediciones correspondientes
    """

    print("Los usuarios existentes son: ")
    for documento in diccionario:
        #print(documento)
        print(documento, diccionario[documento]["usuario"])

    validar = False
    while validar != True:
        user = input("Escriba el documento de identidad del usuario que desea editar: ")
        validar = validar_existencia(user,diccionario)
        if validar == False:
            print("Ingrese un usuario existente")
    
    print("El nombre de usuario actual es:", diccionario[user]["usuario"], ", se le solicitará uno nuevo.")
    nuevo_nombre = nombre_usuario()
    diccionario[user]["usuario"] = nuevo_nombre
    print("La contraseña de usuario actual es:", diccionario[user]["contrasena"], ", se le solicitará una nueva.")
    nueva_password = password("1")
    diccionario[user]["contrasena"] = nueva_password
    print("El rol del usuario actual es:", diccionario[user]["rol"], ", se le solicitará uno nuevo.")
    nuevo_rol = respuesta_numerica("Seleccione el rol 1. Admin y 2. Operador:","12")
    if nuevo_rol == "2": 
        diccionario[user]["rol"] = "Operador"
    elif nuevo_rol == "1":
        diccionario[user]["rol"] = "Administrador"

def eliminar_usuario(diccionario):
    
    """
    Sinopsis:
        Función que hace uso de otras funciones creadas anteriormente permite eliminar usuarios
        de un diccionario.


    Entradas y salidas:
        - diccionario: diccionario que se usará como base para consultar la existencia de documentos 
        y eliminar usuarios.
        - returns: none solo se hacen las eliminaciones correspondientes
    """

    print("Los usuarios existentes son: ")
    for documento in diccionario:
        print(documento, diccionario[documento]["usuario"])
              
    validar = False
    while validar != True:
        user = input("Escriba el documento de identidad del usuario que desea eliminar: ")
        validar = validar_existencia(user,diccionario)
        if validar == False:
            print("Ingrese un usuario existente")
    
    del diccionario[user]

#------------------------------- Modulos Creación, Gestión y Eliminación de Estaciones -------------------------------#

def crear_estacion(diccionario):

    """
    Sinopsis:
        funcion que usa el diccionario y crea una nueva estación dentro en base
        a la información ingresada por el usuario.


    Entradas y salidas:
        - diccionario: diccionario de estaciones
        - returns: none, y la actualización del diccionario tomado como paramétro 
        con la información de la nueva estación. 
    """

    municipios = ["Medellín","Bello","Itagui","Caldas","La Estrella","Barbosa"]
    nombre_estacion = input("Ingrese el nombre de la estacion, (Es valido cualquier caracter):")
    codigo = generar_codigo(diccionario)
    print("--------------------------")
    indice = 1
    for i in municipios:
        print(i, "("+str(indice)+")" )
        indice += 1
    print("--------------------------")    
    modo = respuesta_numerica("Seleccione uno de los municipios disponibles:\n","123456")
    modo = int(modo)

    diccionario[codigo] = {'nombre_estacion': nombre_estacion, 'municipio': municipios[modo-1], "mediciones": [] }

def editar_estacion(diccionario):
    
    """
    Sinopsis:
        funcion que usa el diccionario y nos permite editar la información 
        de alguna de las estaciones 


    Entradas y salidas:
        - diccionario: diccionario de estaciones
        - returns: none, y la actualización del diccionario con la nueva 
        información. 
    """

    municipios = ["Medellín","Bello","Itagui","Caldas","La Estrella","Barbosa"]
    print("Los códigos de las estaciones existentes son: ")
    for estacion in diccionario:
        print(estacion, diccionario[estacion]["nombre_estacion"])
    
    validar = False
    while validar != True:
        codigo = input("Escriba el código de la estacion que desea editar:")
        validar = validar_existencia(codigo,diccionario)
        if validar == False:
            print("Ingrese un codigo de estación existente")

    print("El nombre de la estación es:", diccionario[codigo]["nombre_estacion"], ", se le solicitará uno nuevo.")
    nnombre_estacion = input("Ingrese el nombre de la estacion, (Es valido cualquier caracter):")
    diccionario[codigo]["nombre_estacion"] = nnombre_estacion
    print("El municipio actual es:", diccionario[codigo]["municipio"], ", se le solicitará uno nuevo.\n")

    indice = 1
    for i in municipios:
        print(i, "("+str(indice)+")" )
        indice += 1
    print("--------------------------")    
    modo = respuesta_numerica("Seleccione uno de los municipios disponibles:\n","123456")
    modo = int(modo)

def eliminar_estacion(diccionario):

    """
    Sinopsis:
        funcion que usa el diccionario y nos permite editar una estación existente.

    Entradas y salidas:
        - diccionario: diccionario de estaciones
        - returns: none, y la actualización del diccionario. 
    """

    print("Las estaciones existentes son: ")
    for codigo in diccionario:
        print(codigo, diccionario[codigo]["nombre_estacion"])

    validar = False
    while validar != True:
        codigo = input("Escriba el codigo de la estacion que desea eliminar: ")
        validar = validar_existencia(codigo,diccionario)
        if validar == False:
            print("Ingrese un usuario existente")

    del diccionario[codigo]

#------------------------------- Modulos Para Extracción de Información de la Base de Datos -------------------------------# 
  
def extraer_usuarios(archivo,diccionario):

    """
    Sinopsis:
        funcion que nos extraer los usuarios del archivo registros.txt siguiendo de manera estricto 
        las caracteristicas de la plantilla

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - diccionario: diccionario donde se almacenará la información
        - returns: none: se realizan los cambios durante la ejecución
    """

    file = open_file(archivo)
    indice = 0
    maxIndice = posLineWhite(archivo)[0]
    
    while indice != maxIndice:
        linea = file[indice].rstrip()
        linea = linea[1:-1]
        elementos = custom_split(linea,";")
        diccionario[elementos[0]] = {'usuario': elementos[1], 'contrasena': elementos[2], 'rol': elementos[3]}
        indice += 1

def extraer_departamentos(archivo):

    """
    Sinopsis:
        funcion que nos extrae los departamentos disponibles de la base de datos

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - returns: lista con departamentos
    """

    file = open_file(archivo)
    indice = posLineWhite(archivo)[1]-1
    linea = file[indice].rstrip()
    linea = linea[1:]
    departamentos = custom_split(linea,",")
    return departamentos

def extraer_estaciones(archivo,diccionario):

    """
    Sinopsis:
        funcion que nos extraer las estaciones del diccionario

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - returns: lista con departamentos
    """

    file = open_file(archivo)
    indice = posLineWhite(archivo)[1]+1
    maxIndice = posLineWhite(archivo)[2]-1 
    linea = file[indice].rstrip()

    while indice != maxIndice:
        linea = file[indice].rstrip()
        elementos = custom_split(linea,",")
        diccionario[elementos[0]] = {'nombre_estacion': elementos[1], 'municipio': elementos[2], 'mediciones': [] }
        indice += 1 

def extraer_tipo_variable(archivo):

    """
    Sinopsis:
        funcion que nos extrae los tipos de variables disponibles a analizar de la base de datos

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - returns: lista con los tipos de variable
    """

    file = open_file(archivo)
    indice = posLineWhite(archivo)[2]+1
    linea = file[indice].rstrip()
    lecturas = custom_split(linea,";")
    return lecturas

def extraer_lecturas(archivo):

    """
    Sinopsis:
        funcion que nos extraer las lecturas de la base de datos

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - returns: lista con las lecturas
    """

    file = open_file(archivo)
    indice = posLineWhite(archivo)[3]+1
    maxIndice = len(file)
    lista = []

    while indice != maxIndice:
        linea = file[indice].rstrip()
        lista.append(custom_split(linea,";"))
        indice += 1
    return lista

#------------------------------- Modulos Para Gestión de Mediciones -------------------------------#

def input_valor_validacion(string):
    
    """
    Sinopsis:
        Función que permite validar si un string se puede castear a un número valido
        , si el se toma como argumento ND o ND la funcion retorna ND


    Entradas y salidas:
        - string: valor para hacer la comprobación
        - returns: numero en entero o ND en ese caso particular
    """

    caracteres = "0123456789NDnd"
    contCaracteresInvalidos = 1

    while contCaracteresInvalidos != 0:
        contCaracteresInvalidos = 0
        p = input(string, )
        for caracter in p:
            if caracter not in caracteres:
                contCaracteresInvalidos += 1
                print("Ingrese un valor valido")
                break
        if p.upper() == "ND":
            return "ND" 
    return int(p)

def input_variable_medicion(tipo):

    """
    Sinopsis:
        funcion que permite solicitar al usuario que ingrese cualquiera de los 4 tipos 
        de variables. 


    Entradas y salidas:
        - tipo: tipo de variable denotado con un numero:
        0.PM25
        1.PM30
        2.Temperatura
        3.Humedad
        - returns: valor de la variable
    """

    estado = True
    if tipo == 0:
        while estado:
            var = input_valor_validacion("Ingrese la medición de PM10 en ug/m3:")   
            if type(var) == str:
                return -999
            if validacion_rango(var,0.0,100.0):
                estado = False
            if validacion_rango(var,0.0,100.0) == False:
                print("Ingrese un valor valido")

    elif tipo == 1:
        while estado:
            var = input_valor_validacion("Ingrese la medición de PM25 en ug/m3:")
            if type(var) == str:
                return -999
            if validacion_rango(var,0.0,200.0):
                estado = False
            if validacion_rango(var,0.0,200.0) == False:
                print("Ingrese un valor valido")

    elif tipo == 2:
        while estado:
            var = input_valor_validacion("Ingrese la medición de temperatura en °C:")
            if type(var) == str:
                return -999
            if validacion_rango(var,-20.0,50.0):
                estado = False
            if validacion_rango(var,-20,50.0) == False:
                print("Ingrese un valor valido")

    elif tipo == 3:
        while estado:
            var = input_valor_validacion("Ingrese la medición de humedad en %:")
            if type(var) == str:
                return -999
            if validacion_rango(var,0.0,100.0):
                estado = False
            if validacion_rango(var,0.0,100.0) == False:
                print("Ingrese un valor valido")

    return var

#------------------------------- Modulos Para Escritura de Informacion en Archivo.txt -------------------------------#

def write_text(dic_usuarios,dic_estaciones,list_municipios,list_tipos_mediciones,mediciones):

    """
    Sinopsis:
        Función permite escribir la información en un archivo.txt siguiendo una estructura de manera estricta.


    Entradas y salidas:
    dic_usuarios: diccionario con los usuarios
    dic_estaciones: diccionario con las estaciones
    list_municipios: lista de municipios
    list_tipos_mediciones: listas con todos los tipos de variables
    mediciones: las mediciones estarán dentro de las estaciones, pero se ingresaran por separado.
    return: none

    """

    mUsuarios = []
    mMunicipios = []
    mEstacion = []
    mTMedida = ['PM10[0.0:100.0,ug/m3]', 'PM25[0.0:200.0,ug/m3]', 'Temperatura[-20.0:50.0,Â°C]', 'Humedad[0.0:100.0,%]']
    mMediciones = []
    mTMedicion = []
    Aux = ":"

    for clave, valor in dic_usuarios.items():
        usuarios = f"<{clave};{valor['usuario']};{valor['contrasena']};{valor['rol']}>"
        mUsuarios.append(usuarios)

    for municipio in list_municipios:
        linea = f"{Aux}{municipio}"
        mMunicipios.append(linea)
        Aux = ","

    for clave,valor in dic_estaciones.items():
        estacion = f"{clave},{valor['nombre']},{valor['municipio']} "
        mEstacion.append(estacion)

    AuxWhile = 0
    while AuxWhile != len(list_tipos_mediciones)-1:
        linea = f"{list_tipos_mediciones[AuxWhile]},"
        mTMedicion.append(linea)
        AuxWhile += 1
    linea = f"{list_tipos_mediciones[AuxWhile]}"
    mTMedicion.append(linea)

    iterador = 0
    iteradorS = 0
    for medicion in mediciones:
        linea = f"{medicion[iteradorS]},{medicion[iteradorS+1]},{medicion[iteradorS+2]}"
        mMediciones.append(linea)
        iteradorS = 0



    # Escribir las líneas en un archivo de texto
    with open("datos.txt", "w") as archivo:

        for linea in mUsuarios:
            archivo.write(f"{linea}\n")
        archivo.write("\n")

        for linea in mMunicipios:
            archivo.write(f"{linea}")
        archivo.write("\n\n")    

        for linea in mEstacion:
            archivo.write(f"{linea}\n")
        archivo.write("\n")

        for linea in mTMedicion:
            archivo.write(f"{linea}")
        archivo.write("\n")
        archivo.write("\n")

        for linea in mMediciones:
            archivo.write(f"{linea}\n")
        archivo.write("\n")

#------------------------------- Funciones para ingreso y lectura de datos -------------------------------#

def registrar_mediciones(diccionario,codigo):

    """
    Sinopsis:
        Función permite ingresar mediciones como valor a un diccinario previamente definido


    Entradas y salidas:
        - diccionario: diccionario base
        - codigo: codigo de la estacion
        - returns: no hay, se realizan las actualizaciones correspondientes
    """

    dirM = {}
    lista_valores = []
    fecha = obtener_fecha()
    lista_mediciones = []

    for i in range(0,4):
        valores = input_variable_medicion(i)
        lista_valores.append(valores)
    
    dirM[fecha] = {'PM10':lista_valores[0],'PM25':lista_valores[1],'temperatura':lista_valores[2],'humedad':lista_valores[3]}
    lista_mediciones.append(dirM)

    diccionario[codigo]["mediciones"] += lista_mediciones

def listar_mediciones(dir_estaciones,codigo):

    """
    Sinopsis:
        Función permite listar las medidas de cierta estación dada como código


    Entradas y salidas:
        - dir_estaciones: diccionario base
        - codigo: codigo de la estacion
        - returns: no hay, se realizan las impresiones de la información correspondiente

        -ES POSIBLE QUE SE HAGAN CAMBIOS EN LA FUNCION PARA IMPRIMIR LA INFORMACION EN TABLAS, PERO POR AHORA PERMANECE ASÍ
    """

    mediciones = dir_estaciones[codigo]["mediciones"]
    for i in mediciones:
        for j in i:
            print("El día:", j, "las mediciones fueron:", "PM10:", i[j]["PM10"], "PM25:", i[j]["PM25"], "Temperatura", i[j]["temperatura"], "Humedad", i[j]["humedad"])

#------------------------------- Menus del sistema preliminares -------------------------------#

def inicio_de_sesion(diccionario):

    """
    Sinopsis:
        Función que permite simula un inicio de sesion en una plataforma en base al documento de identidad
        ,nombre de usuario y contraseña.


    Entradas y salidas:
        - diccionario: diccionario base que contiene la información
        - returns: none
        - Si se autoriza el ingreso, se redirige la ejecución a otra funcion

    """

    mode = respuesta_numerica("Seleccione: \n""1.Usuario Registrado\n""2.Usuario Visitante\n","12")
    
    if mode == "1":
        ID = identificacion()
        comprobacion = validar_existencia(ID,diccionario)
        if comprobacion:
            print("El documento se ha encontrado en la base de datos")
            usuario = nombre_usuario()
            contrasena = password(1)
            if diccionario[ID]['usuario'] == usuario:
                if diccionario[ID]['contrasena'] == contrasena:
                    print("Has iniciado sesion exitosamente")
                    limpiar_pantalla()
            else:
                print("Nombre de usuario errado")
        else:
            print("El documento no se ha encontrado en la base de datos")
    elif mode == "2":
        print("Cargando Estadisticas............")
        limpiar_pantalla()



#----------------------------------------------------- Analisis de Estadisticas -------------------------------------------------#

def MedicionFechaEspecifica(diccionario, variable, fecha):

    """
    Sinopsis:
        Función permite ver el valor de una variable en un día especifico


    Entradas y salidas:
        - diccionario: diccionario base que contiene la información
        - variable: variable que se desea consultar
        - fecha: fecha que se desea consultar
        - returns: none, se imprime el valor de la variable en la fecha seleccionada

    """

    valores = []
    for medicion in diccionario['1']['mediciones']:
        fecha_str = list(medicion.keys())[0]
        if fecha_str == fecha:
            medicion_actual = medicion[fecha_str]
            if variable in medicion_actual:
                valor_actual = medicion_actual[variable]
                if valor_actual != -999:
                    valores.append(valor_actual)
    print(f'El valor de la {variable} para la fecha {fecha} es {valores[0]}')


def MNpromedioDias(diccionario, variable, codigo_estacion,opcion):

    """
    Sinopsis:
        Función que permite encontrar el valor promedio, minimo y maáximo de cierta variable en los
        últimos 7 o 30 días


    Entradas y salidas:
        - diccionario: diccionario base que contiene la información
        - variable: variable que se desea consultar
        - codigo_estacion: codigo de la estación de la que se desea consultar la información
        - returns: none, se imprime los valores promedio, máximos y minimos de cierta variable

    """
    
    fechas = []
    for medicion in diccionario[codigo_estacion]["mediciones"]:
        fecha = list(medicion.keys())[0]
        fechas.append(fecha)

    if opcion == 1:
        valores = []
        for i in reversed(range(-7, 0)):
            fecha = fechas[i]
            valor = diccionario[codigo_estacion]["mediciones"][i][fecha][variable]
            if valor == -999:
                pass
            else:
                valores.append(valor)
        promedio = ((sumList(valores))/len(valores))
        minimo = valorMin(valores) 
        maximo = valorMax(valores)
        print(f'El promedio de la variable {variable} en los ultimos 7 dias es {promedio} el minimo es {minimo} y el maximo es {maximo}')

            
    elif opcion == 2:
        valores = []
        for i in reversed(range(-30,0)):
            fecha = fechas[i]
            valor = diccionario[codigo_estacion]["mediciones"][i][fecha][variable]
            if valor == -999:
                pass
            else:
                valores.append(valor)
        promedio = ((sumList(valores))/len(valores))
        minimo = valorMin(valores) 
        maximo = valorMax(valores)
        print(f'El promedio de la variable {variable} en los ultimos 30 dias es {promedio} el minimo es {minimo} y el maximo es {maximo}')