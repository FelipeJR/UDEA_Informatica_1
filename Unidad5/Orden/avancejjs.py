from utilidadesA import *
from funcionesPrincipales import *
from gestionUsuariosEstaciones import *

usuarios = {}
estaciones = {
    '1': {
        'nombre_estacion': 'Universidad San Buenaventura',
        'municipio': 'Medellin',
        'mediciones': {
            '2019-07-01 00:00:00': {
                'PM10': '3.5',
                'PM25': '6.2',
                'Temperatura': '27.0',
                'Humedad': '34.0'
            },
            '2019-07-01 00:00:10': {
                'PM10': '3.6',
                'PM25': '6.2',
                'Temperatura': '27.0',
                'Humedad': '34.0'
            },
            '2019-07-01 00:00:11': {
                'PM10': '3.7',
                'PM25': '6.2',
                'Temperatura': '27.0',
                'Humedad': '34.0'
            },
            '2019-07-01 00:00:12': {
                'PM10': '3.8',
                'PM25': '6.2',
                'Temperatura': '27.0',
                'Humedad': '34.0'
            }
        }
    }
}

def mediciones_lista(diccionario,codigo):
    lista = []
    indice = 0
    for i in diccionario[codigo]["mediciones"]: 
        lista.append([codigo])
        lista[indice].append(i)
        lista[indice].append((diccionario[codigo]["mediciones"][i]["PM10"]))
        lista[indice].append((diccionario[codigo]["mediciones"][i]["PM25"]))
        lista[indice].append((diccionario[codigo]["mediciones"][i]["Temperatura"]))
        lista[indice].append((diccionario[codigo]["mediciones"][i]["Humedad"]))
        indice += 1
    return lista


def escoger_variables():
    listSel = [False, False, False, False]  # Lista inicial con todas las variables deseleccionadas

    while True:
        opciones = \
            "Para visualizar variables, por favor, elija las opciones que desee y, en caso de que desee deseleccionar alguna, simplemente vuelva a seleccionarla.\n"\
            "1. PM10: " + ("[*]" if listSel[0] else "[]") + "\n" \
            "2. PM25: " + ("[*]" if listSel[1] else "[]") + "\n" \
            "3. Temperatura: " + ("[*]" if listSel[2] else "[]") + "\n" \
            "4. Humedad: " + ("[*]" if listSel[3] else "[]") + "\n" \
            "5. Continuar\n"\
            "--------------------------\n"

        modo = respuesta_numerica(opciones, "12345")

        if modo == "1":
            listSel[0] = not listSel[0]
        elif modo == "2":
            listSel[1] = not listSel[1]
        elif modo == "3":
            listSel[2] = not listSel[2]
        elif modo == "4":
            listSel[3] = not listSel[3]
        elif modo == "5":
            break

        clean_screen()

    return listSel



def VisualizarEstadisticas(diccionario,estaciones,modo):

    """
    Función que permite visualizar las estadisticas de cierta variable

    Entradas y salidas
        - estaciones (dict): Diccionario que contiene la información de las estaciones

    Returns:
        None. Imprime la información solicitada por el usuario

    """

    clean_screen()
    print("Seleccione el código de una de las siguientes estaciones:\n ")
    for i in estaciones:
        estacion = estaciones[i]["nombre_estacion"]
        print(i,estacion)
    print("--------------------------")

    nDis = opciones_validas(estaciones)
    estacion = respuesta_numerica("Seleccione la Estacion: ",nDis)
    clean_screen()
    var = escoger_variables()

    opciones = \
           "Escoja el tipo de Analisis\n"\
           "1. Últimos 7 días\n" \
           "2. Últimos 30 días\n" \
           "3. Fecha Especifica\n" \
           "--------------------------\n"
    clean_screen()
    modoEstadistica = respuesta_numerica(opciones, "123")
    

    if modoEstadistica == "1":
        print("1")
    elif modoEstadistica == "2":
        print("2")
    elif modoEstadistica == "3":
        print("3")

    opciones = \
           "¿Qué desea realizar?\n"\
           "1. Retroceder\n" \
           "2. Salir\n" \
           "--------------------------\n"
    clean_screen()
    modoA = respuesta_numerica(opciones, "12")

    if modoA == "1":
        clean_screen()
        inicio_de_sesion(usuarios,estaciones)
        
    elif modo == "2":
        clean_screen()
        exit()



VisualizarEstadisticas(usuarios,estaciones,1)












#encabezado = ['Código de Estación', 'Fecha de Medición', 'PM10', 'PM25', 'Temperatura', 'Humedad']
#ancho_columnas = [20, 20, 10, 19, 15, 10]
#imprimir_tabla(datos, ancho_columnas, encabezado)


