#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  4 04:35:34 2023

@author: practi06
"""
def extraer_estaciones(archivo,diccionario):

    """
    Sinopsis:
        funcion que nos extraer las estaciones del diccionario

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - returns: lista con departamentos
    """

    file = open_file(archivo)
    indice = posLineWhite(archivo)[1]+1   #Desde la primera linea en blanco que va a extraer las posiciones
    maxIndice = posLineWhite(archivo)[2]-1  #Hasta la ultima linea en blanco que va a extraer posiciones
    linea = file[indice].rstrip()
    
    lecturas =  extraer_lecturas(archivo)
    
    #####################################
    #Aqui simplemente llamamos los tipos de variable para meterlos en una lista para meterlos al diccionario de estaciones
    #para que no quede asi por si depronto cambian la variable de medicion en el txt
    #diccionario[clave]['mediciones'][lecturas[i][0]] = {"PM10" : variables1[0] , "PM25":variables1[1],"Temperatura" : variables1[2],"Humedad" : variables1[3], }
    tiposDeVariables = extraer_tipo_variable(archivo)
    #¨print(tiposDeVariables)
    bandera = True
    acumulador=""
    listaVariables = []
    
    for i in tiposDeVariables:
        for j in i:
            if j == "]":
                bandera = True
            elif j == "[":
                bandera = False
                listaVariables.append(acumulador)
                acumulador = ""
            elif bandera:
                acumulador = acumulador + j
            
    #####################################¨
    
    #####################################
    while indice != maxIndice:
        linea = file[indice].rstrip()
        elementos = custom_split(linea,",")
       
        diccionario[elementos[0]] = {'nombre_estacion': elementos[1], 'municipio': elementos[2], 'mediciones':{}}
        indice += 1 
    contador=1
    
    #Relacionaamos las lecturas que tenemos o mediciones con las estaciones
    for clave in diccionario: 
        for i in range(len(lecturas)):   
            id = lecturas[i][1] 
            if clave == id:
                diccionario[clave]['mediciones'][lecturas[i][0]] = {} #Creamos una clave (es la fecha o fechas) para meter las variables
                variables = lecturas[i][2]   
                variables1 = custom_split(variables[1:-1],",") 
                #diccionario[clave]['mediciones'][lecturas[i][0]] = {listaVariables[0] : variables1[0] , "PM25":variables1[1],"Temperatura" : variables1[2],"Humedad" : variables1[3], }
                diccionario[clave]['mediciones'][lecturas[i][0]] = {listaVariables[0] : variables1[0] , listaVariables[1]:variables1[1],listaVariables[2]: variables1[2],listaVariables[3]: variables1[3], }
