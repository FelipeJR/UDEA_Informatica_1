usuarios = {
'1035972061': {
'usuario': 'ufelipejr',
'contrasena': 'fjrsh',
'rol': 'Administrador'
}
}
estaciones = {
'1': {
'nombre': 'GIGA',
'municipio': 'La Estrella'
}
}
municipios = ["La Estrella","Medallo","Itagui","Envigado"]
tipos_mediciones = ['PM10[0.0:100.0,ug/m3]', 'PM25[0.0:200.0,ug/m3]', 'Temperatura[-20.0:50.0,Â°C]', 'Humedad[0.0:100.0,%]']
mediciones = [
    ['2019-07-01 00:00:00', '1', '{3.5,6.2,27.0,34.0}'],
    ['2019-07-01 00:10:00', '2', '{8.1,-999.0,29.0,37.0}'],
    ['2019-07-01 00:00:00', '3', '{7.9,-999.0,31.0,-999.0}'],
    ['2019-04-01 00:00:00', '4', '{-999.0,6.4,-999.0,41.0}']
    ]




def write_text(dic_usuarios,dic_estaciones,list_municipios,list_tipos_mediciones,mediciones):
    mUsuarios = []
    mMunicipios = []
    mEstacion = []
    mTMedida = ['PM10[0.0:100.0,ug/m3]', 'PM25[0.0:200.0,ug/m3]', 'Temperatura[-20.0:50.0,Â°C]', 'Humedad[0.0:100.0,%]']
    mMediciones = []
    mTMedicion = []
    Aux = ":"

    for clave, valor in dic_usuarios.items():
        usuarios = f"<{clave};{valor['usuario']};{valor['contrasena']};{valor['rol']}>"
        mUsuarios.append(usuarios)

    for municipio in municipios:
        linea = f"{Aux}{municipio}"
        mMunicipios.append(linea)
        Aux = ","

    for clave,valor in estaciones.items():
        estacion = f"{clave},{valor['nombre']},{valor['municipio']} "
        mEstacion.append(estacion)

    AuxWhile = 0
    while AuxWhile != len(list_tipos_mediciones)-1:
        linea = f"{list_tipos_mediciones[AuxWhile]},"
        mTMedicion.append(linea)
        AuxWhile += 1
    linea = f"{list_tipos_mediciones[AuxWhile]}"
    mTMedicion.append(linea)

    iterador = 0
    iteradorS = 0
    for medicion in mediciones:
        linea = f"{medicion[iteradorS]},{medicion[iteradorS+1]},{medicion[iteradorS+2]}"
        mMediciones.append(linea)
        iteradorS = 0



    # Escribir las líneas en un archivo de texto
    with open("datos.txt", "w") as archivo:

        #for linea in mUsuarios:
        #    archivo.write(f"{linea}\n")
        #archivo.write("\n")

        #for linea in mMunicipios:
        #    archivo.write(f"{linea}")
        #archivo.write("\n\n")    

        #for linea in mEstacion:
        #    archivo.write(f"{linea}\n")
        #archivo.write("\n")

        for linea in mTMedicion:
            archivo.write(f"{linea}")
        archivo.write("\n")

        #for linea in mMediciones:
        #    archivo.write(f"{linea}\n")
        #archivo.write("\n")

write_text(usuarios,estaciones,municipios,tipos_mediciones,mediciones)
