#-------------------------------------- Extracciones  -------------------------------------#
def extraer_usuarios(archivo,diccionario):

    """
    Sinopsis:
        funcion que nos extraer los usuarios del archivo registros.txt siguiendo de manera estricto 
        las caracteristicas de la plantilla

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - diccionario: diccionario donde se almacenará la información
        - returns: none: se realizan los cambios durante la ejecución
    """

    file = open_file(archivo)
    indice = 0
    maxIndice = posLineWhite(archivo)[0]
    
    while indice != maxIndice:
        linea = file[indice].rstrip()
        linea = linea[1:-1]
        elementos = custom_split(linea,";")
        diccionario[elementos[0]] = {'usuario': elementos[1], 'contrasena': elementos[2], 'rol': elementos[3]}
        indice += 1

def extraer_departamentos(archivo):

    """
    Sinopsis:
        funcion que nos extrae los departamentos disponibles de la base de datos

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - returns: lista con departamentos
    """

    file = open_file(archivo)
    indice = posLineWhite(archivo)[1]-1
    linea = file[indice].rstrip()
    linea = linea[1:]
    departamentos = custom_split(linea,",")
    return departamentos


def extraer_estaciones(archivo, diccionario):
    """
    Sinopsis:
        Función que extrae las estaciones del diccionario.

    Entradas y salidas:
        - archivo: archivo del cual se extraerá la información.
        - diccionario: diccionario donde se almacenarán las estaciones.
        - returns: lista con estaciones.
    """
    file = open_file(archivo)
    indice = posLineWhite(archivo)[1] + 1  # Desde la primera línea en blanco se extraerán las posiciones
    maxIndice = posLineWhite(archivo)[2]  # Hasta la última línea en blanco se extraerán posiciones

    lecturas = extraer_lecturas(archivo)

    tiposDeVariables = extraer_tipo_variable(archivo)
    listaVariables = []

    for i in tiposDeVariables:
        bandera = True  # Inicializamos la variable bandera
        acumulador = ""
        for j in i:
            if j == "]":
                bandera = True
            elif j == "[":
                bandera = False
                listaVariables.append(acumulador)
                acumulador = ""
            elif bandera:
                acumulador = acumulador + j

    while indice != maxIndice:
        linea = file[indice].rstrip()
        elementos = custom_split(linea, ",")

        diccionario[elementos[0]] = {'nombre_estacion': elementos[1], 'municipio': elementos[2], 'mediciones': {}}
        indice += 1

    # Verificamos que la lista lecturas no esté vacía antes de acceder a los elementos
    if lecturas:
        for i in range(len(lecturas)):
            if len(lecturas[i]) >= 3:  # Verificamos que haya suficientes elementos en la lista
                id = lecturas[i][1]
                if id in diccionario:
                    diccionario[id]['mediciones'][lecturas[i][0]] = {}
                    variables = lecturas[i][2]
                    variables1 = custom_split(variables[1:-1], ",")
                    for j in range(len(listaVariables)):
                        diccionario[id]['mediciones'][lecturas[i][0]][listaVariables[j]] = variables1[j]

    return diccionario




def extraer_tipo_variable(archivo):

    """
    Sinopsis:
        funcion que nos extrae los tipos de variables disponibles a analizar de la base de datos

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - returns: lista con los tipos de variable
    """

    file = open_file(archivo)
    indice = posLineWhite(archivo)[2]+1
    linea = file[indice].rstrip()
    lecturas = custom_split(linea,";")
    return lecturas

def extraer_lecturas(archivo):

    """
    Sinopsis:
        funcion que nos extraer las lecturas de la base de datos

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - returns: lista con las lecturas
    """

    file = open_file(archivo)
    indice = posLineWhite(archivo)[3]+1
    maxIndice = len(file)
    lista = []

    while indice != maxIndice:
        linea = file[indice].rstrip()
        lista.append(custom_split(linea,";"))
        indice += 1
    return lista



#-------------------------------------- Gestión de Usuarios -------------------------------------#

def identificacion():
    
    """
    Sinopsis:
        Función que le solicita al usuario que ingrese un documento de identidad y valida si cumple 
        con los requerimientos.


    Entradas y salidas:
        - None
        - returns: documento 
    """

    documento = " "
    while validar_identificacion(documento) != True:
        documento = input("Ingrese documento de identidad del usuario: ")
        if validar_identificacion(documento) != True:
            print("Ingrese un documento valido")

    return documento 

def nombre_usuario():

    """
    Sinopsis:
        Función que le solicita al usuario el nombre de usuario para la plataforma y valida 
        si es valido, es decir que solo tenga letras y espacios, por otra parte convierte las
        mayusculas a minusculas.


    Entradas y salidas:
        - None
        - returns: nombre_usuario
    """

    nombreU = "1"
    while validar_nombre_usuario(nombreU) != True:
        nombreU = input("Ingrese el nombre de usuario: ").lower()
        if validar_nombre_usuario(nombreU) != True:
            print("El nombre de usuario solo puede contener letras y espacios, intenta nuevamente.")
    return nombreU

def password(modo):

    """
    Sinopsis:
        Funcion que le solicita al usuario que ingrese una contraseña, verifica 
        si es valida y la retorna; por otra parte tiene dos modos:
        1. Para escribir la contraseña una sola vez
        2. Para escribir la contraseña y confirmarla


    Entradas y salidas:
        - modo: 1 o 2 dependiendo de lo que se requiera
        - returns: passwordA
    """

    passwordA = ""

    if modo == 1:
        while validar_password(passwordA) != True:
            passwordA = input("Ingrese la contraseña: ")
            if validar_password(passwordA) != True:
                print("Ingrese una contraseña de minimo 4 caracteres")

    elif modo == 2:
        comprobacion = False
        while validar_password(passwordA) != True or comprobacion != True:
            passwordA = input("Ingrese la contraseña: ")
            if validar_password(passwordA) != True:
                print("Ingrese una contraseña de minimo 4 caracteres")
            else:
                passwordB = input("Confirme su contraseña: ")
                if passwordA == passwordB:
                    comprobacion = True
                else:
                    print("Las credenciales son diferentes, ingrese la contraseña y confirme nuevamente")
    return passwordA

def crear_usuario(diccionario,estaciones,Idusuario):

    """
    Sinopsis:
        Función que hace uso de otras funciones creadas anteriormente y recopila el documento de 
        identificación, contraseña y rol para después retornar un diccionario agregando esta 
        información.


    Entradas y salidas:
        - diccionario: diccionario que se usará como base para consultar la existencia de documentos y 
        se actualizará con los nuevos usuarios. 
        - returns: none solo se hacen las ediciones correspondientes
    """

    ID = list(diccionario.keys())[0]
    while validar_existencia(ID, diccionario):
        ID = identificacion()
        if validar_existencia(ID, diccionario):
            print("Este documento de identidad ya está registrado en la base de datos")

    user = nombre_usuario()
    contrasena = password(2)
    rol = respuesta_numerica("Seleccione el rol 1. Admin y 2. Operador:","12")

    if rol == "2":
        diccionario[ID] = {'usuario': user, 'contrasena': contrasena, 'rol': 'Operador'}
    elif rol == "1":
        diccionario[ID] = {'usuario': user, 'contrasena': contrasena, 'rol': 'Administrador'}
    print("El usuario se ha creado con exito")
    sleep(2)
    clean_screen()
    AccionesAdmin(diccionario,estaciones,Idusuario)

def editar_usuario(diccionario,estaciones,idusuario):

    """
    Sinopsis:
        Función que hace uso de otras funciones creadas anteriormente, permite editar usuarios existentes
        en un diccionario


    Entradas y salidas:
        - diccionario: diccionario que se usará como base para consultar la existencia de documentos y 
        edición de usuarios 
        - returns: none solo se hacen las ediciones correspondientes
    """

    print("Los usuarios existentes son: ")
    for documento in diccionario:
        #print(documento)
        print(documento, diccionario[documento]["usuario"])

    validar = False
    while validar != True:
        print("Escriba el documento de identidad del usuario que desea editar: ")
        user = identificacion()
        validar = validar_existencia(user,diccionario)
        if validar == False:
            print("Ingrese un usuario existente")
    



    print("El nombre de usuario actual es:", diccionario[user]["usuario"], ", se le solicitará uno nuevo.")
    nuevo_nombre = nombre_usuario()
    diccionario[user]["usuario"] = nuevo_nombre
    print("La contraseña de usuario actual es:", diccionario[user]["contrasena"], ", se le solicitará una nueva.")
    nueva_password = password(2)
    diccionario[user]["contrasena"] = nueva_password
    print("El rol del usuario actual es:", diccionario[user]["rol"], ", se le solicitará uno nuevo.")
    nuevo_rol = respuesta_numerica("Seleccione el rol 1. Admin y 2. Operador:","12")
    if nuevo_rol == "2": 
        diccionario[user]["rol"] = "Operador"
    elif nuevo_rol == "1":
        diccionario[user]["rol"] = "Administrador"
        
    sleep(2)
    clean_screen()
    AccionesAdmin(diccionario,estaciones,idusuario)

def eliminar_usuario(diccionario,estaciones,Idusuario):
    
    """
    Sinopsis:
        Función que hace uso de otras funciones creadas anteriormente permite eliminar usuarios
        de un diccionario.


    Entradas y salidas:
        - diccionario: diccionario que se usará como base para consultar la existencia de documentos 
        y eliminar usuarios.
        - returns: none solo se hacen las eliminaciones correspondientes
    """

    print("Los usuarios existentes son: ")
    for documento in diccionario:
        print(documento, diccionario[documento]["usuario"])

    # print(Idusuario)
              
    validar = False
    while validar != True:
        user = input("Escriba el documento de identidad del usuario que desea eliminar: ")
        validar = validar_existencia(user,diccionario)
        if validar == False:
            print("Ingrese un usuario existente")
    
    #print(user)
    #print(Idusuario)
        
    if user == Idusuario:
        print("No puedes eliminar tu propio usuario")
        sleep(2)
        return 

    r = respuesta_numerica("¿Desea continuar? 1.(Si), 2.(No)","12")

    if r == "1":
        del diccionario[user]
        print("El usuario se ha eliminado con éxito")
        sleep(2)
        clean_screen()
        AccionesAdmin(diccionario,estaciones,Idusuario)
    elif r == "2":
        clean_screen()
        eliminar_usuario(diccionario,estaciones)
    
    


#-------------------------------------- Gestión de Estaciones -------------------------------------#
def crear_estacion(diccionario):

    """
    Sinopsis:
        funcion que usa el diccionario y crea una nueva estación dentro en base
        a la información ingresada por el usuario.


    Entradas y salidas:
        - diccionario: diccionario de estaciones
        - returns: none, y la actualización del diccionario tomado como paramétro 
        con la información de la nueva estación. 
    """

    municipios = ["Medellín","Bello","Itagui","Caldas","La Estrella","Barbosa"]
    nombre_estacion = input("Ingrese el nombre de la estacion, (Es valido cualquier caracter):")
    codigo = generar_codigo(diccionario)
    print("--------------------------")
    indice = 1
    for i in municipios:
        print(i, "("+str(indice)+")" )
        indice += 1
    print("--------------------------")    
    modo = respuesta_numerica("Seleccione uno de los municipios disponibles:\n","123456")
    modo = int(modo)

    diccionario[codigo] = {'nombre_estacion': nombre_estacion, 'municipio': municipios[modo-1], "mediciones": {} }

def editar_estacion(diccionario):
    
    """
    Sinopsis:
        funcion que usa el diccionario y nos permite editar la información 
        de alguna de las estaciones 


    Entradas y salidas:
        - diccionario: diccionario de estaciones
        - returns: none, y la actualización del diccionario con la nueva 
        información. 
    """

    municipios = ["Medellín","Bello","Itagui","Caldas","La Estrella","Barbosa"]
    print("Los códigos de las estaciones existentes son: ")
    for estacion in diccionario:
        print(estacion, diccionario[estacion]["nombre_estacion"])
    
    validar = False
    while validar != True:
        codigo = input("Escriba el código de la estacion que desea editar:")
        validar = validar_existencia(codigo,diccionario)
        if validar == False:
            print("Ingrese un codigo de estación existente")


    print("El municipio de la estación:", diccionario[codigo]["municipio"], "Se le solicitará uno nuevo.")

    clean_screen()
    print("El nombre de la estación es:", diccionario[codigo]["nombre_estacion"], ", se le solicitará uno nuevo.")
    nnombre_estacion = input("Ingrese el nombre de la estacion, (Es valido cualquier caracter):")
    diccionario[codigo]["nombre_estacion"] = nnombre_estacion
    
    clean_screen()
    print("El municipio actual es:", diccionario[codigo]["municipio"], ", se le solicitará uno nuevo.")

    indice = 1
    for i in municipios:
        print(i, "("+str(indice)+")" )
        indice += 1
    print("--------------------------")    
    modo = respuesta_numerica("Seleccione uno de los municipios disponibles:\n","123456")

    if modo == "1":
        diccionario[codigo]["municipio"] = "Medellin"
    elif modo == "2":
        diccionario[codigo]["municipio"] = "Bello"
    elif modo == "3":
        diccionario[codigo]["municipio"] = "Itagui"
    elif modo == "4":
        diccionario[codigo]["municipio"] = "Caldas"
    elif modo == "5":
        diccionario[codigo]["municipio"] = "La Estrella"
    elif modo == "6":
        diccionario[codigo]["municipio"] = "Barbosa"

def eliminar_estacion(diccionario):

    """
    Sinopsis:
        funcion que usa el diccionario y nos permite editar una estación existente.

    Entradas y salidas:
        - diccionario: diccionario de estaciones
        - returns: none, y la actualización del diccionario. 
    """

    print("Las estaciones existentes son: ")
    for codigo in diccionario:
        print(codigo, diccionario[codigo]["nombre_estacion"])

    validar = False
    while validar != True:
        codigo = input("Escriba el codigo de la estacion que desea eliminar: ")
        validar = validar_existencia(codigo,diccionario)
        if validar == False:
            print("Ingrese un codigo de estacíón valido")
    
    #Soft Delete
    #print(diccionario[codigo]["mediciones"])
    
    if diccionario[codigo]["mediciones"] == {}:
        del diccionario[codigo]
        print("Estacion eliminada correctamente.")
        sleep(2)
        eliminar_estacion(diccionario)

    else:
        print("No se puede eliminar la estacion porque tiene mediciones registradas")
        sleep(2)
        eliminar_estacion(diccionario)

#-------------------------------------- Menus -------------------------------------#
def gestionarUsuarios(usuarios,estaciones,Idusuario):
    clean_screen()
    opciones = \
           "1. Crear Usuario\n" \
           "2. Eliminar Usuario\n" \
           "3. Editar Usuario\n" \
           "4. Retroceder\n"\
           "5. Finalizar Ejecución\n"\
           "--------------------------\n"
    modo = respuesta_numerica(opciones,"12345")

    if modo == "1":
        crear_usuario(usuarios,estaciones,Idusuario)
    elif modo == "2":
        eliminar_usuario(usuarios,estaciones,Idusuario)
    elif modo == "3":
        editar_usuario(usuarios,estaciones,Idusuario)
    elif modo == "4":
        AccionesAdmin(usuarios,estaciones,Idusuario)
    elif modo == "5":
        finalizar_ejecucion(usuarios,estaciones,"registros.txt")

def gestionarEstaciones(usuarios,estaciones,Idusuario):
    clean_screen()
    opciones = \
           "1. Crear Estación\n" \
           "2. Eliminar Estación\n" \
           "3. Editar Estación\n" \
           "4. Retroceder\n"\
           "5. Finalizar Ejecución\n"\
           "--------------------------\n"
    modo = respuesta_numerica(opciones,"12345")
    if modo == "1":
        crear_estacion(estaciones)
    elif modo == "2":
        eliminar_estacion(estaciones)
    elif modo == "3":
        editar_estacion(estaciones)
    elif modo == "4":
        AccionesAdmin(usuarios,estaciones,Idusuario)
    elif modo == "5":
        finalizar_ejecucion(usuarios,estaciones,"registros.txt")



def AccionesAdmin(usuarios,estaciones,Idusuario):
    clean_screen()
    print("Como usuario administrador puedes realizar las siguientes acciones:")

    opciones = \
           "1. Gestionar Usuarios\n" \
           "2. Gestionar Estaciones\n" \
           "3. Visualizar Estadisticas\n"\
           "4. Retroceder\n"\
           "5. Finalizar Ejecución\n"\
           "--------------------------\n"
    
    modo = respuesta_numerica(opciones,"12345")

    if modo == "1":
        gestionarUsuarios(usuarios,estaciones,Idusuario)
    elif modo == "2":
        gestionarEstaciones(usuarios,estaciones,Idusuario)
    elif modo == "3":
        VisualizarEstadisticas(usuarios,estaciones)
    elif modo == "4":
        inicio_de_sesion(usuarios,estaciones,Idusuario)
    elif modo == "5":
        finalizar_ejecucion(usuarios,estaciones,"registros.txt")

    
        
def inicio_de_sesion(usuarios,estaciones,Idusuario):

    """
    Sinopsis:
        Función que permite simula un inicio de sesion en una plataforma en base al documento de identidad
        ,nombre de usuario y contraseña.


    Entradas y salidas:
        - diccionario: diccionario base que contiene la información
        - returns: none
        - Si se autoriza el ingreso, se redirige la ejecución a otra funcion

    """
    clean_screen()
    print("Bienvenido a la Gestion de Variables Ambientales V1.23.15")
    mode = respuesta_numerica("Seleccione: \n""1.Usuario Registrado\n""2.Usuario Visitante\n""3.Finalizar Ejecución\n","123")
    
    if mode == "1":
        ID = identificacion()
        comprobacion = validar_existencia(ID,usuarios)
        if comprobacion:
            print("El documento se ha encontrado en la base de datos")
            contrasena = password(1)
            if usuarios[ID]['contrasena'] == contrasena:
                usuario = usuarios[ID]["usuario"]
                ##Sacamos el id del usuario para pasarlo a las otras funciones para poder comparar el id del user que ingreso con el que va a eliminar
                Idusuario = ID
                # print(Idusuario)
                print(f"Has iniciado sesion exitosamente {usuario}")
                sleep(2)
                animacion(".",40,0.01)
                clean_screen()

                if usuarios[ID]["rol"] == "Administrador":
                    print("Tu rol es Administrador")
                    AccionesAdmin(usuarios,estaciones,Idusuario)


                elif usuarios[ID]["rol"] == "Operador":
                    print("Tu rol es Operador")
                    AccionesOperador(usuarios,estaciones,Idusuario)
            else:
                print("Nombre de usuario errado")
                sleep(2)
                clean_screen()
        else:
            print("El documento no se ha encontrado en la base de datos")
            sleep(2)

            
    elif mode == "2":
        print("Cargando Estaciones..."),sleep(2), animacion(".",40,0.01)
        clean_screen()
        VisualizarEstadisticas(usuarios,estaciones,Idusuario)

    elif mode == "3":
        finalizar_ejecucion(usuarios,estaciones,"registros.txt")

def AccionesOperador(usuarios,estaciones,Idusuario):
    clean_screen()

    opciones = \
           "Su tipo de usuario es operador\n"\
           "1. Operar Estación\n" \
           "2. Visualizar Estadisticas\n" \
           "3. Volver a menú inicial\n" \
           "4. Finalizar Ejecución\n" \
           "--------------------------\n"
    
    modo = respuesta_numerica(opciones,"1234")
    if modo == "1":
        OperarEstacion(usuarios,estaciones,Idusuario)
    elif modo == "2":
        VisualizarEstadisticas(usuarios,estaciones,Idusuario)
    elif modo == "3":
        inicio_de_sesion(usuarios,estaciones,Idusuario)
    elif modo == "4":
        finalizar_ejecucion(usuarios,estaciones,"registros.txt")

     



def OperarEstacion(usuarios,estaciones,Idusuario):
    clean_screen()
    mun = escoger_municipio()
    listmuni = []

    if mun[0] == True:
        listmuni.append("Medellin")
    if mun[1] == True:
        listmuni.append("Bello")
    if mun[2] == True:
        listmuni.append("Itagui")
    if mun[3] == True:
        listmuni.append("Caldas")
    if mun[4] == True:
        listmuni.append("La Estrella")
    if mun[5] == True:
        listmuni.append("Barbosa")

    clean_screen()
    num_dispo = ""
    print("Estas son las estaciones que se encuentran en el municipio seleccionado:")
    for i in estaciones:
        if estaciones[i]["municipio"] in listmuni:
            num_dispo += i
            nombre_e = estaciones[i]["nombre_estacion"]
            print(f"{i}. {nombre_e}")
    menu = int(num_dispo[-1])+1
    print(f"{menu}. Volver al menú de Operador ")

    station = respuesta_numerica("Seleccione el código o el número correspondiente al retroceso:", num_dispo + str(menu))
    codigo_estacion = str(station)

    if station == str(menu):
        AccionesOperador(usuarios,estaciones,Idusuario)

    medicionesE = estaciones[codigo_estacion]["mediciones"]
    clean_screen()
    print("Medidas Actuales")
    mostrar_tabla_todas_variables(medicionesE)

    sleep(2)
    print("Ingrese las nuevas medidas")
    input_variable_medicion(estaciones,codigo_estacion)

    opciones = \
        "1. Volver a menú operador\n" \
        "2. Volver a inicio de sesión\n" \
        "3. Finalizar Ejecución\n"\
        "--------------------------\n"
    clean_screen()
    b = respuesta_numerica(opciones, "132")

    if b == "1":
        AccionesOperador(usuarios,estaciones,Idusuario)
    elif b == "2":
        inicio_de_sesion(usuarios,estaciones,Idusuario) 
    elif b == "3":
        finalizar_ejecucion(usuarios,estaciones,"registros.txt")


def MNpromedioDiasX(estaciones, opcion, modo):

    if modo == 2:
        with open("estadisticas.txt", "r+", encoding="utf-8") as archivo:  # Abre el archivo en modo de lectura y escritura
            archivo.truncate(0)  

    clean_screen()
    departamentos = escoger_municipio()
    listM = []
    listEstaciones = []
    
    if departamentos[0]:
        listM.append("Medellin")
    if departamentos[1]:
        listM.append("Bello")
    if departamentos[2]:
        listM.append("Itagui")
    if departamentos[3]:
        listM.append("Caldas")
    if departamentos[4]:
        listM.append("La Estrella")
    if departamentos[5]:
        listM.append("Barbosa")

    if opcion == 1:
        opcion = -7
    else:
        opcion = -30

    for i in estaciones:
        mun = estaciones[i]["municipio"]
        if mun in listM:
            listEstaciones.append(estaciones[i])

    clean_screen()
    listV = []

    variables = escoger_variables()
    if variables[0]:
        listV.append("PM10")
    if variables[1]:
        listV.append("PM25")
    if variables[2]:
        listV.append("Temperatura")
    if variables[3]:
        listV.append("Humedad")

    # Tabla PM10
    listaPM10 = []
    listaPM25 = []
    listaTemperatura = []
    listaHumedad = []



    for estacion in listEstaciones[opcion:]:
        mediciones_estacion = estacion['mediciones']
        for fecha, mediciones in mediciones_estacion.items():
            medicion_PM10 = mediciones.get('PM10')
            medicion_PM25 = mediciones.get('PM25')
            temperatura = mediciones.get('Temperatura')
            humedad = mediciones.get('Humedad')
            if medicion_PM10 is not None:
                listaPM10.append([estacion['nombre_estacion'], fecha, medicion_PM10])
            if medicion_PM25 is not None:
                listaPM25.append([estacion['nombre_estacion'], fecha, medicion_PM25])
            if temperatura is not None:
                listaTemperatura.append([estacion['nombre_estacion'], fecha, temperatura])
            if humedad is not None:
                listaHumedad.append([estacion['nombre_estacion'], fecha, humedad])


    Vmin = 1000000000000
    estacionMin = ""
    horaMedMin = ""

    vMax = 0
    estacionMax = ""
    horaMedMax = ""

    promedio = 0
    suma = 0
    contador = 0

    Vmin_PM25 = 1000000000000
    estacionMin_PM25 = ""
    horaMedMin_PM25 = ""

    vMax_PM25 = 0
    estacionMax_PM25 = ""
    horaMedMax_PM25 = ""

    promedio_PM25 = 0
    suma_PM25 = 0
    contador_PM25 = 0

    Tmin = 1000000000000
    estacionMin_T = ""
    horaMedMin_T = ""

    Tmax = 0
    estacionMax_T = ""
    horaMedMax_T = ""

    promedio_T = 0
    suma_T = 0
    contador_T = 0

    Hmin = 1000000000000
    estacionMin_H = ""
    horaMedMin_H = ""

    Hmax = 0
    estacionMax_H = ""
    horaMedMax_H = ""

    promedio_H = 0
    suma_H = 0
    contador_H = 0


    for i in listaPM10:
        medicion = float(i[2])  
        if medicion != -999:
            if medicion < Vmin:
                Vmin = medicion
                estacionMin = i[0]
                horaMedMin = i[1]
            if medicion > vMax:
                vMax = medicion
                estacionMax = i[0]
                horaMedMax = i[1]
            suma += medicion
            contador += 1

    if contador > 0:
        promedio = suma / contador


    for i in listaPM25:
        medicion_PM25 = float(i[2])  
        if medicion_PM25 != -999:
            if medicion_PM25 < Vmin_PM25:
                Vmin_PM25 = medicion_PM25
                estacionMin_PM25 = i[0]
                horaMedMin_PM25 = i[1]
            if medicion_PM25 > vMax_PM25:
                vMax_PM25 = medicion_PM25
                estacionMax_PM25 = i[0]
                horaMedMax_PM25 = i[1]
            suma_PM25 += medicion_PM25
            contador_PM25 += 1

    if contador_PM25 > 0:
        promedio_PM25 = suma_PM25 / contador_PM25

    for i in listaTemperatura:
        medicion_T = float(i[2])  
        if medicion_T != -999:
            if medicion_T < Tmin:
                Tmin = medicion_T
                estacionMin_T = i[0]
                horaMedMin_T = i[1]
            if medicion_T > Tmax:
                Tmax = medicion_T
                estacionMax_T = i[0]
                horaMedMax_T = i[1]
            suma_T += medicion_T
            contador_T += 1

    if contador_T > 0:
        promedio_T = suma_T / contador_T

    for i in listaHumedad:
        medicion_H = float(i[2])  
        if medicion_H != -999:
            if medicion_H < Hmin:
                Hmin = medicion_H
                estacionMin_H = i[0]
                horaMedMin_H = i[1]
            if medicion_H > Hmax:
                Hmax = medicion_H
                estacionMax_H = i[0]
                horaMedMax_H = i[1]
            suma_H += medicion_H
            contador_H += 1

    if contador_H > 0:
        promedio_H = suma_H / contador_H


    for i in listV:

        if i == "PM10":

            TablaPM10 = [
                ["Estación con mínima medición", estacionMin],
                ["Mínima Medición", Vmin],
                ["Fecha y Hora de Medición", horaMedMin],
                ["Estación con máxima medición", estacionMax],
                ["Máxima Medición", vMax],
                ["Fecha y Hora de Medición", horaMedMax],
                ["Promedio de los municipios", promedio]
            ]

            encabezado = ["Variables", "Valores e información PM10"]
            ancho = [40, 40]

            if modo == 1:
                imprimir_tabla(TablaPM10, ancho, encabezado)
    
            elif modo == 2:
                with open("estadisticas.txt", "a", encoding="utf-8") as archivo:
                    archivo.write("Información de PM10:\n\n")
                    for item in TablaPM10:
                        linea = ": ".join(str(elem) for elem in item)
                        archivo.write(linea + "\n")
                    archivo.write("\n")

        elif i == "PM25":

            TablaPM25 = [
                ["Estación con mínima medición", estacionMin_PM25],
                ["Mínima Medición", Vmin_PM25],
                ["Fecha y Hora de Medición", horaMedMin_PM25],
                ["Estación con máxima medición", estacionMax_PM25],
                ["Máxima Medición", vMax_PM25],
                ["Fecha y Hora de Medición", horaMedMax_PM25],
                ["Promedio de los municipios", promedio_PM25]
            ]

            encabezado_PM25 = ["Variables", "Valores e información PM25"]
            ancho_PM25 = [40, 40]

            if modo == 1:
                imprimir_tabla(TablaPM25, ancho_PM25, encabezado_PM25)

            elif modo == 2:
                with open("estadisticas.txt", "a", encoding="utf-8") as archivo:
                    archivo.write("Información de PM25:\n\n")
                    for item in TablaPM25:
                        linea = ": ".join(str(elem) for elem in item)
                        archivo.write(linea + "\n")
                    archivo.write("\n")

        elif i == "Temperatura":
            TablaTemperatura = [
                ["Estación con mínima medición", estacionMin_T],
                ["Mínima Medición", Tmin],
                ["Fecha y Hora de Medición", horaMedMin_T],
                ["Estación con máxima medición", estacionMax_T],
                ["Máxima Medición", Tmax],
                ["Fecha y Hora de Medición", horaMedMax_T],
                ["Promedio de los municipios", promedio_T]
            ]

            encabezado_T = ["Variables", "Valores e información Temperatura"]
            ancho_T = [40, 40]

            if modo == 1:
                imprimir_tabla(TablaTemperatura, ancho_T, encabezado_T)

            elif modo == 2:
                with open("estadisticas.txt", "a", encoding="utf-8") as archivo:
                    archivo.write("Información de Temperatura:\n\n")
                    for item in TablaTemperatura:
                        linea = ": ".join(str(elem) for elem in item)
                        archivo.write(linea + "\n")
                    archivo.write("\n")
            
        elif i == "Humedad":

            TablaHumedad = [
                ["Estación con mínima medición", estacionMin_H],
                ["Mínima Medición", Hmin],
                ["Fecha y Hora de Medición", horaMedMin_H],
                ["Estación con máxima medición", estacionMax_H],
                ["Máxima Medición", Hmax],
                ["Fecha y Hora de Medición", horaMedMax_H],
                ["Promedio de los municipios", promedio_H]
            ]

            encabezado_H = ["Variables", "Valores e información Humedad"]
            ancho_H = [40, 40]

            if modo == 1:
                imprimir_tabla(TablaHumedad, ancho_H, encabezado_H)

            elif modo == 2:
                with open("estadisticas.txt", "a", encoding="utf-8") as archivo:
                    archivo.write("Información de Humedad:\n\n")
                    for item in TablaHumedad:
                        linea = ": ".join(str(elem) for elem in item)
                        archivo.write(linea + "\n")
                    archivo.write("\n")
            
        if modo == 2:
            print("Se ha guardado la información en el archivo de texto estadisticas.txt")
            sleep(1)
            clean_screen()

def MedicionFechaEspecifica(estaciones,modo):

    """
    Sinopsis:
        Función permite ver el valor de una variable en un día especifico


    """

    if modo == 2:
        with open("estadisticas.txt", "r+", encoding="utf-8") as archivo:  # Abre el archivo en modo de lectura y escritura
            archivo.truncate(0)
    
    clean_screen()
    departamentos = escoger_municipio()
    listM = []
    listEstaciones = []
    listV = []
    tabla = []  
    listDatos = [] 
    plantillaDatos = [] 
    listV.append("Fecha")
    listV.append("Hora")
    listV.append("Codigo Estación")

    if departamentos[0]:
        listM.append("Medellin")
    if departamentos[1]:
        listM.append("Bello")
    if departamentos[2]:
        listM.append("Itagui")
    if departamentos[3]:
        listM.append("Caldas")
    if departamentos[4]:
        listM.append("La Estrella")
    if departamentos[5]:
        listM.append("Barbosa")


    for i in estaciones:
        mun = estaciones[i]["municipio"]
        if mun in listM:
            listEstaciones.append(estaciones[i])

    clean_screen()

    

    variables = escoger_variables()
    if variables[0]:
        listV.append("PM10")
    if variables[1]:
        listV.append("PM25")
    if variables[2]:
        listV.append("Temperatura")
    if variables[3]:
        listV.append("Humedad")
    
    fecha = solicitar_fecha()
    
    for i in estaciones:
        for j in estaciones[i]["mediciones"]:
            fechaAux = j[0:10]
            horaAux = j[11:16]
            if fechaAux == fecha and estaciones[i]["municipio"] in listM:
                segmento = [
                    fechaAux,
                    horaAux,
                    i,
                    estaciones[i]["mediciones"][j]
                ]
                
                listDatos.append(segmento)


    if modo == 1:
        for d in listDatos:
            tabla = [
                d[0],  
                d[1],  
                d[2],
                *(d[3]['PM10'] for _ in [0] if variables[0] and 'PM10' in d[3]),
                *(d[3]['PM25'] for _ in [0] if variables[1] and 'PM25' in d[3]),
                *(d[3]['Temperatura'] for _ in [0] if variables[2] and 'Temperatura' in d[3]),
                *(d[3]['Humedad'] for _ in [0] if variables[3] and 'Humedad' in d[3])
            ]
            plantillaDatos.append(tabla)


    elif modo == 2:
        for d in listDatos:
            tabla = [
                "Fecha de Medición: " + d[0],  
                "Hora de Medición: " + d[1],
                "Código de la Estación: " + d[2],
                "PM10: " + (str(d[3]['PM10']) if variables[0] and 'PM10' in d[3] else ""),
                "PM25: " + (str(d[3]['PM25']) if variables[1] and 'PM25' in d[3] else ""),
                "Temperatura: " + (str(d[3]['Temperatura']) if variables[2] and 'Temperatura' in d[3] else ""),
                "Humedad: " + (str(d[3]['Humedad']) if variables[3] and 'Humedad' in d[3] else "")
            ]
            plantillaDatos.append("\n".join(tabla))



    ancho = []
    while len(ancho) != len(listV):
        ancho.append(15)
    
    
    if modo == 1:
        imprimir_tabla(plantillaDatos, ancho, listV)

    elif modo == 2:
        with open("estadisticas.txt", "w", encoding="utf-8") as file:
            file.write("Información de la fecha ingresada:\n\n")
            file.write("\n\n".join(plantillaDatos))
        print("Se ha guardado la información en el archivo de texto estadisticas.txt")
        sleep(1)
        clean_screen()

def VisualizarEstadisticas(usuarios,estaciones,Idusuario):

    """
    Función que permite visualizar las estadisticas de cierta variable

    Entradas y salidas
        - estaciones (dict): Diccionario que contiene la información de las estaciones

    Returns:
        None. Imprime la información solicitada por el usuario

    """    
    clean_screen()
    opciones = \
           "Escoja el tipo de Analisis\n"\
           "1. Últimos 7 días\n" \
           "2. Últimos 30 días\n" \
           "3. Fecha Especifica\n" \
           "--------------------------\n"
    

    modoEstadistica = respuesta_numerica(opciones, "123")

    if modoEstadistica == "1":
        pregunta = respuesta_numerica("¿Desea visualizar la información en tablas (1.) o en un archivo de texto (2.)?: ", "12")
        if pregunta == "1":
            MNpromedioDiasX(estaciones, 1, 1)
        elif pregunta == "2":
            MNpromedioDiasX(estaciones, 1, 2)

    elif modoEstadistica == "2":
        pregunta = respuesta_numerica("¿Desea visualizar la información en tablas (1.) o en un archivo de texto (2.)?: ", "12")
        if pregunta == "1":
            MNpromedioDiasX(estaciones, 2, 1)
        elif pregunta == "2":
            MNpromedioDiasX(estaciones, 2, 2)

    elif modoEstadistica == "3":
        pregunta = respuesta_numerica("¿Desea visualizar la información en tablas (1.) o en un archivo de texto (2.)?: ", "12")
        if pregunta == "1":
            MedicionFechaEspecifica(estaciones,1)
        elif pregunta == "2":
            MedicionFechaEspecifica(estaciones,2)

    pregunta = respuesta_numerica("Desea volver al menú principal (1.) o continuar visualizando estadisticas (2.)?: ", "12")
    if pregunta == "1":
        inicio_de_sesion(usuarios,estaciones,Idusuario)
    elif pregunta == "2":
        VisualizarEstadisticas(usuarios,estaciones,Idusuario)
    
      

def mostrar_tabla_todas_variables(diccionario):
    lista = []
    for i in diccionario:
        tabla = [
            i[0:10],
            i[10:20],
            diccionario[i]["PM10"],
            diccionario[i]["PM25"],
            diccionario[i]["Temperatura"],
            diccionario[i]["Humedad"]
        ]
        lista.append(tabla)
    
    encabezado = ["Fecha","Hora","PM10","PM25","Temperatura","Humedad"]
    ancho = [15, 13, 12, 10, 12, 8]
    imprimir_tabla(lista, ancho, encabezado)

    ##Esta funcion hay que analizarla y acomodarla ya que si eliminamos alguna 

def escoger_municipio():
    municipios = extraer_departamentos("registros.txt")
    listSel = [False, False, False, False, False, False]

    while True:
        opciones = (
            "Seleccione los municipios que desea analizar.\n"
            f"1. {municipios[0]}: " + ("[*]" if listSel[0] else "[]") + "\n"
            f"2. {municipios[1]}: " + ("[*]" if listSel[1] else "[]") + "\n"
            f"3. {municipios[2]}: " + ("[*]" if listSel[2] else "[]") + "\n"
            f"4. {municipios[3]}: " + ("[*]" if listSel[3] else "[]") + "\n"
            f"5. {municipios[4]}: " + ("[*]" if listSel[4] else "[]") + "\n"
            f"6. {municipios[5]}: " + ("[*]" if listSel[5] else "[]") + "\n"
            "7. Continuar\n"
            "--------------------------\n"
        )
        modo = respuesta_numerica(opciones, "1234567")

        if modo == "1":
            listSel[0] = not listSel[0]
        elif modo == "2":
            listSel[1] = not listSel[1]
        elif modo == "3":
            listSel[2] = not listSel[2]
        elif modo == "4":
            listSel[3] = not listSel[3]
        elif modo == "5":
            listSel[4] = not listSel[4]
        elif modo == "6":
            listSel[5] = not listSel[5]
        elif modo == "7":
            break

        clean_screen()
    return listSel

def escoger_variables():
    listSel = [False, False, False, False]  # Lista inicial con todas las variables deseleccionadas

    while True:
        opciones = \
            "Para visualizar variables, por favor, elija las opciones que desee y, en caso de que desee deseleccionar alguna, simplemente vuelva a seleccionarla.\n"\
            "1. PM10: " + ("[*]" if listSel[0] else "[]") + "\n" \
            "2. PM25: " + ("[*]" if listSel[1] else "[]") + "\n" \
            "3. Temperatura: " + ("[*]" if listSel[2] else "[]") + "\n" \
            "4. Humedad: " + ("[*]" if listSel[3] else "[]") + "\n" \
            "5. Continuar\n"\
            "--------------------------\n"

        modo = respuesta_numerica(opciones, "12345")

        if modo == "1":
            listSel[0] = not listSel[0]
        elif modo == "2":
            listSel[1] = not listSel[1]
        elif modo == "3":
            listSel[2] = not listSel[2]
        elif modo == "4":
            listSel[3] = not listSel[3]
        elif modo == "5":
            break

        clean_screen()

    return listSel

def open_file(archivo):
    """
    Sinopsis:
    Función que permite abrir un archivo dado como argumento y cargarlo en memoria

    Entradas y salidas:
    - archivo: nombre del archivo
    - returns: lineas
    """
    with open(archivo) as file:
        lineas = file.readlines()
    return lineas

    
def posLineWhite(archivo):

    """
    Sinopsis:
        funcion que permite calcular la posición de las lineas que está completamente en blanco de un archivo

    Entradas y salidas:
        - archivo: nombre del archivo
        - returns: posLineWhite 
    """

    lectura = open_file(archivo) 
    posLineWhite = []
    pos = 0

    for linea in lectura:
        linea_limpia = linea.rstrip()
        if len(linea_limpia) == 0:
            posLineWhite.append(pos)
        pos += 1

    return posLineWhite

def sleep(nSegundos):

    """
    Sinopsis:
        Función permite simular el pausado de la ejecución del código


    Entradas y salidas:
        nSegundos: segundos en los que se pausará el codigo
        returns: none

    """

    from datetime import datetime
    tiempo_inicio = datetime.now()
    while (datetime.now() - tiempo_inicio).total_seconds() < nSegundos:
        pass

def clean_screen():
    import sys
    import os
    """
    Sinopsis:
        Función que permite limpiar la pantalla


    Entradas y salidas:
        - returns: none, solo se limpia la pantalla
    """

    if sys.platform.startswith('win'):
        os.system('cls')
    else:
        print("\033c", end="")


def search_position(linea, separadores):
    """
    Sinopsis:
        Función que nos permite encontrar las ubicaciones exactas de los separadores en el string ingresado.

    Entradas y salidas:
        - linea: string a evaluar.
        - separadores: delimitador a buscar.
        - returns: pos: posiciones.
    """
    pos = []
    for i in range(len(linea)):
        if linea[i] in separadores:
            pos.append(i)
    return pos


def custom_split(string, separadores):
    """
    Sinopsis:
        Función que nos permite dividir un string en elementos utilizando los separadores dados.

    Entradas y salidas:
        - string: string a dividir.
        - separadores: delimitadores para dividir el string.
        - returns: list_elementos: lista de elementos obtenidos.
    """
    pos = search_position(string, separadores)
    list_elementos = []

    if len(pos) == 0:
        # Si no se encuentra ningún separador, devolvemos el string completo
        list_elementos.append(string)
        return list_elementos

    # Agregamos el primer elemento antes del primer separador
    list_elementos.append(string[:pos[0]])

    # Iteramos sobre los separadores encontrados para obtener los elementos
    for i in range(len(pos)):
        if i < len(pos) - 1:
            # Si no es el último separador, agregamos el elemento entre los separadores
            indice0 = pos[i] + 1
            indice1 = pos[i + 1]
            list_elementos.append(string[indice0:indice1])
        else:
            # Si es el último separador, agregamos el elemento después del último separador
            indice0 = pos[i] + 1
            list_elementos.append(string[indice0:])

    return list_elementos



def respuesta_numerica(string,digitos_validos_str):

    """
    Sinopsis:
        Función que permite validar si el usuario ingresa un número de un 
        conjunto especifico dado como argumento


    Entradas y salidas:
        - digitos_validos_str: conjunto de números validos en str
        - string: texto que se mostrará por pantalla al ejecutar el input
        - returns: num
    """

    num = ""
    while num not in digitos_validos_str or len(num) != 1 or len(num) != 1:
        num = input(string)
        if num not in digitos_validos_str or len(num) != 1:
            print("¡La opción no es válida!")
            print("╔═══════════════════════╗")
            print("║ Seleccione nuevamente ║")
            print("╚═══════════════════════╝")
            

    return num

def generar_codigo(diccionario):

    """
    Sinopsis:
        funcion que usa el diccionarion de estaciones, itera entre todos las claves que 
        corresponden a los códigos y retorna un número mayor que el código mayor del diccionario.


    Entradas y salidas:
        - diccionario: diccionario de estaciones
        - returns: nuevo código compatible con los requerimientos
    """

    if len(diccionario) == 0:
        return "1"
    
    mayor = 0
    for clave in diccionario:
        if int(clave) > mayor:    
            mayor = int(clave)
        cod = mayor + 1
    return str(cod)

def animacion(caracter,tamaño,tiempo):
    maximo = 0
    while maximo != tamaño:
        print(caracter*maximo)
        sleep(tiempo)
        clean_screen()
        maximo += 1

def solicitar_fecha():

    """
    Sinopsis:
        Función que le solicita al usuario una fecha y verifica si es o no valida


    Entradas y salidas:
        - returns: fecha si la misma es valida 
    """

    estado = False
    while estado != True:
        fecha = input("Ingrese la fecha en el formato YYYY-MM-DD: ")
        estado = validar_fecha(fecha)
        if estado != True:
            print("Ingrese una fecha valida")
    return fecha

def obtener_fecha():

    """
    Sinopsis:
        Función que permite obtener la fecha exacta del dispositivo


    Entradas y salidas:
        - returns: ahora, fecha del equipo
    """

    from datetime import datetime
    ahora = str(datetime.now())
    return ahora[0:19]

def input_variable_medicion(estaciones,codigo):
    """
    Sinopsis:
    Función que permite solicitar al usuario que ingrese cualquiera de los 4 tipos de variables.

    Entradas y salidas:
    - tipo: tipo de variable denotado con un número:
      0.PM25
      1.PM30
      2.Temperatura
      3.Humedad
    - returns: valor de la variable
    """
    
    diccionario = {}

    estado = True

    fecha = obtener_fecha()

    tiposMed = extraer_tipo_variable("registros.txt")


    while estado:
        PM10 = input_valor_validacion(f"Ingrese {tiposMed[0]} en los rangos dados: ")   
        if type(PM10) == str:
            PM10 = -999
            break
        if validacion_rango(PM10, 0.0, 100.0):
            estado = False
        if not validacion_rango(PM10, 0.0, 100.0):
            print("Ingrese un valor válido")

    estado = True
    while estado:
        PM25 = input_valor_validacion(f"Ingrese {tiposMed[1]} en los rangos dados: ")
        if type(PM25) == str:
            PM25 = -999
            break
        if validacion_rango(PM25, 0.0, 200.0):
            estado = False
        if not validacion_rango(PM25, 0.0, 200.0):
            print("Ingrese un valor válido")

    estado = True
    while estado:
        t = input_valor_validacion(f"Ingrese {tiposMed[2]} en los rangos dados: ")
        if type(t) == str:
            t = -999
            break
        if validacion_rango(t, -20.0, 50.0):
            estado = False
        if not validacion_rango(t, -20, 50.0):
            print("Ingrese un valor válido")

    estado = True
    while estado:
        h = input_valor_validacion(f"Ingrese {tiposMed[3]} en los rangos dados: ")
        if type(h) == str:
            h = -999
            break
        if validacion_rango(h, 0.0, 100.0):
            estado = False
        if not validacion_rango(h, 0.0, 100.0):
            print("Ingrese un valor válido")

    diccionario[fecha] = {"PM10": PM10, "PM25": PM25, "Temperatura": t, "Humedad": h}
    estaciones[codigo]['mediciones'].update(diccionario)
    print("Medidas guardadas correctamente en memoria")
    sleep(2)

def input_valor_validacion(string):
    
    """
    Sinopsis:
        Función que permite validar si un string se puede castear a un número valido
        , si el se toma como argumento ND o ND la funcion retorna ND


    Entradas y salidas:
        - string: valor para hacer la comprobación
        - returns: numero en entero o ND en ese caso particular
    """

    caracteres = "0123456789NDnd"
    contCaracteresInvalidos = 1

    while contCaracteresInvalidos != 0:
        contCaracteresInvalidos = 0
        p = input(string, )
        for caracter in p:
            if caracter not in caracteres:
                contCaracteresInvalidos += 1
                print("Ingrese un valor valido")
                break
        if p.upper() == "ND":
            return "ND" 
    return int(p)

def validar_fecha(fecha):
    """
    Sinopsis:
        Función que permite validar si una fecha está en el formato dado


    Entradas y salidas:
        - fecha: fecha que será validada
        - returns: True o False dependiendo de si es valida
    """
    
    import datetime
    try:
        datetime.datetime.strptime(fecha, '%Y-%m-%d')
        return True
    except ValueError:
        return False
    
def validacion_rango(valor, minimo, maximo):

    """
    Sinopsis:
        Función que permite validar si un número está en un rango dado


    Entradas y salidas:
        - valor: valor para hacer la comprobación
        - minimo: valor minimo del rango
        - maximo: valor maximo del rango
        - returns: True o False dependiendo de si es valido o no
    """

    if valor >= minimo and valor <= maximo or valor == "ND":
        return True
    else:
        return False

def validar_existencia(documento,diccionario):

    """
    Sinopsis:
        Función que comprueba la existencia de una clave en un diccionario 

    Entradas y salidas:
        - documento: documento que se buscará en la base de datos
        - diccionario: diciconario donde se buscará el documento 
        - returns: num
    """

    for clave in diccionario:
        if documento in clave:
            return True
    return False

def validar_identificacion(documento):

    """
    Sinopsis:
        Función que permite validar si un documento de identidad tiene únicamente 10 digitos.


    Entradas y salidas:
        - documento: documento de identidad en str
        - returns: True o False dependiendo de si es valido
    """

    contDigitos = 0
    digitos = "0123456789"

    while contDigitos != 10:
        #contDigitos = 0
        for d in documento:
            if d in digitos:
                contDigitos += 1
            
            if contDigitos != 10 and len(documento) != 10 or d not in digitos:
                return False
    return True

def validar_nombre_usuario(nombre):
    
    """
    Sinopsis:
        Función que permite validar si un nombre de usuario es valido, es decir que solo tenga 
        letras y espacios.


    Entradas y salidas:
        - nombre: nombre de usuario ingresado por el usuario
        - returns: True o False dependiendo de si es valido
    """

    nombre = nombre.lower()
    cont = 1
    cInvalidos = "0123456789!@#\$%\^&\*\(\)_\+-=\[\]{}\\\|;':\",.<>\?\/"
    while cont != 0:
        for caracter in nombre:
            if caracter in cInvalidos:
                return False
        return True
    
def validar_password(password):

    """
    Sinopsis:
        Función que permite validar si una contraseña de usuario es valida, es decir que tenga
        minimo 4 digitos. 


    Entradas y salidas:
        - password: contraseña ingresada como argumento
        - returns: True o False dependiendo de si es valida
    """

    longitud = len(password)
    if longitud < 4:
        return False
    else:
        return True
    
def imprimir_tabla(tabla, ancho, encabezado=None):  
    ''' 
    Imprime en pantalla un tabla con los datos pasados, ajustado a los tamaños deseados.
    
    Argumentos:
        tabla: Lista que representa la tabla. Cada elemento es una fila
        ancho: Lista con el tamaño deseado para cada columna. Si se especifica
            un entero, todas las columnas quedan de ese tamaño
        encabezado: Lista con el encabezado de la tabla
    '''
    def dividir_fila(ancho,sep='-'):
        '''
        ancho: Lista con el tamaño de cada columna
        se: Caracter con el que se van a formar las líneas que 
            separan las filas
        '''
        linea = ''
        for i in range(len(ancho)):
            linea += ('+'+sep*(ancho[i]-1))
        linea = linea[:-1]+'+'
        print(linea)
        
    def imprimir_celda(texto, impresos, relleno):
        '''
        texto: Texto que se va a colocar en la celda
        impresos: cantidad de caracteres ya impresos del texto
        relleno: cantidad de caracteres que se agregan automáticamente,
            para separar los textos del borde de las celda.
        '''        
        # Imprimir celda
        if type(texto) == type(0.0):
            #print(texto)
            texto = '{:^7.2f}'.format(texto)
            #print(type(texto), texto)
        else:
            texto = str(texto)
        texto = texto.replace('\n',' ').replace('\t',' ')
        if impresos+relleno < len(texto):
            print(texto[impresos:impresos+relleno],end='')
            impresos+=relleno
        elif impresos >= len(texto):
            print(' '*(relleno),end='')
        else:
            print(texto[impresos:], end='')
            print(' '*(relleno-(len(texto) - impresos)),end='')
            impresos = len(texto)
        return impresos
    
    def imprimir_fila(fila):
        '''
        fila: Lista con los textos de las celdas de la fila
        '''
        impresos = []   
        alto = 1
        for i in range(len(fila)):
            impresos.append(0)
            if type(fila[i]) == type(0.0):
                texto = '{:7.2f}'.format(fila[i])
            else:
                texto = str(fila[i])
            alto1 = len(texto)//(ancho[i]-4)
            if len(texto)%(ancho[i]-4) != 0:
                alto1+=1
            if alto1 > alto:
                alto = alto1
        for i in range(alto):
            print('| ',end='')
            for j in range(len(fila)):
                relleno = ancho[j]-3
                if j == len(fila)-1:
                    relleno = ancho[j] -4
                    impresos[j] = imprimir_celda(fila[j], impresos[j], relleno)
                    print(' |')
                else:
                    impresos[j] = imprimir_celda(fila[j], impresos[j], relleno)
                    print(' | ',end='')   
    if not len(tabla) > 0:
        return
    if not type(tabla[0]) is list:
        return
    ncols = len(tabla[0])
    if type(ancho) == type(0):
        ancho = [ancho+3]*ncols 
    elif type(ancho) is list:
        for i in range(len(ancho)):
            ancho[i]+=3
    else:
        print('Error. El ancho debe ser un entero o una lista de enteros')
        return
    assert len(ancho) == ncols, 'La cantidad de columnas no coincide con los tamaños dados'
    ancho[-1] += 1
    if encabezado != None:
        dividir_fila(ancho,'=')
        imprimir_fila(encabezado)
        dividir_fila(ancho,'=')
    else:
        dividir_fila(ancho)
    for fila in tabla:
        imprimir_fila(fila)
        dividir_fila(ancho)

def opciones_validas(diccionario):
    string = ""
    for i in diccionario:
        string += str(i)
    last_p = int(i) + 1 
    string += str(last_p)
    return string









def registrar_mediciones(diccionario,codigo):

    """
    Sinopsis:
        Función permite ingresar mediciones como valor a un diccinario previamente definido


    Entradas y salidas:
        - diccionario: diccionario base
        - codigo: codigo de la estacion
        - returns: no hay, se realizan las actualizaciones correspondientes
    """

    dirM = {}
    lista_valores = []
    fecha = obtener_fecha()
    lista_mediciones = []

    for i in range(0,4):
        valores = input_variable_medicion(i)
        lista_valores.append(valores)
    
    dirM[fecha] = {'PM10':lista_valores[0],'PM25':lista_valores[1],'temperatura':lista_valores[2],'humedad':lista_valores[3]}
    lista_mediciones.append(dirM)

    diccionario[codigo]["mediciones"] += lista_mediciones

def listar_mediciones(dir_estaciones,codigo):

    """
    Sinopsis:
        Función permite listar las medidas de cierta estación dada como código


    Entradas y salidas:
        - dir_estaciones: diccionario base
        - codigo: codigo de la estacion
        - returns: no hay, se realizan las impresiones de la información correspondiente

        -ES POSIBLE QUE SE HAGAN CAMBIOS EN LA FUNCION PARA IMPRIMIR LA INFORMACION EN TABLAS, PERO POR AHORA PERMANECE ASÍ
        -POR SI ALGO
    """

    mediciones = dir_estaciones[codigo]["mediciones"]
    for i in mediciones:
        for j in i:
            print("El día:", j, "las mediciones fueron:", "PM10:", i[j]["PM10"], "PM25:", i[j]["PM25"], "Temperatura", i[j]["temperatura"], "Humedad", i[j]["humedad"])


def write_text(dic_usuarios, dic_estaciones, list_municipios, list_tipos_mediciones, mediciones, nombre_archivo):
    """
    Sinopsis:
        Función permite escribir la información en un archivo.txt siguiendo una estructura de manera estricta.

    Entradas y salidas:
    dic_usuarios: diccionario con los usuarios
    dic_estaciones: diccionario con las estaciones
    list_municipios: lista de municipios
    list_tipos_mediciones: listas con todos los tipos de variables
    mediciones: las mediciones estarán dentro de las estaciones, pero se ingresaran por separado.
    nombre_archivo: nombre del archivo en el que se escribirá la información
    return: none
    """

    mUsuarios = []
    mMunicipios = []
    mEstacion = []
    mTMedida = ['PM10[0.0:100.0,ug/m3]' 'PM25[0.0:200.0,ug/m3]', 'Temperatura[-20.0:50.0,°C]', 'Humedad[0.0:100.0,%]']
    mMediciones = []
    Aux = ":"

    for clave, valor in dic_usuarios.items():
        usuarios = f"<{clave};{valor['usuario']};{valor['contrasena']};{valor['rol']}>"
        mUsuarios.append(usuarios)

    for municipio in list_municipios:
        linea = f"{Aux}{municipio}"
        mMunicipios.append(linea)
        Aux = ","

    for clave, valor in dic_estaciones.items():
        estacion = f"{clave},{valor['nombre_estacion']},{valor['municipio']}"
        mEstacion.append(estacion)

    mTMedicion = []
    AuxWhile = 0
    while AuxWhile != len(list_tipos_mediciones)-1:
        linea = f"{list_tipos_mediciones[AuxWhile]};"
        mTMedicion.append(linea)
        AuxWhile += 1
    linea = f"{list_tipos_mediciones[AuxWhile]}"
    mTMedicion.append(linea)

    
    for medicion in mediciones:
        fecha_hora = medicion[0]
        datos = medicion[1:]
        linea = f"{fecha_hora};{';'.join(datos)}"
        mMediciones.append(linea)

    with open(nombre_archivo, "w") as file:
        file.write("\n".join(mUsuarios))
        file.write("\n\n")
        file.write("".join(mMunicipios))
        file.write("\n\n")
        file.write("\n".join(mEstacion))
        file.write("\n\n")
        file.write("".join(mTMedicion))
        file.write("\n\n")
        file.write("\n".join(mMediciones))

def mediciones_lista(estaciones):
    lista = []

    for clave, valor in estaciones.items():
        for timestamp, medicion in valor['mediciones'].items():
            valores = [str(medicion[k]) for k in medicion]
            medicion_str = '{' + ','.join(valores) + '}'
            datos_medicion = [timestamp, clave, medicion_str]
            lista.append(datos_medicion)

    return lista



def finalizar_ejecucion(dic_usuarios,dic_estaciones,nombre_archivo):
    print("Guardando información en la base de datos ....")
    sleep(1)
    list_municipios = extraer_departamentos("registros.txt")
    list_tipos_mediciones = extraer_tipo_variable("registros.txt")
    #mediciones = extraer_lecturas("registros.txt")
    mediciones = mediciones_lista(dic_estaciones)
    write_text(dic_usuarios,dic_estaciones,list_municipios,list_tipos_mediciones,mediciones,nombre_archivo)
    print("Información almacenada en la base de datos con éxito.....")
    sleep(1)
    clean_screen()
    exit()