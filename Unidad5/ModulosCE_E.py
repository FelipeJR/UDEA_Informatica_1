def crear_estacion(diccionario):

    """
    Sinopsis:
        funcion que usa el diccionario y crea una nueva estación dentro en base
        a la información ingresada por el usuario.


    Entradas y salidas:
        - diccionario: diccionario de estaciones
        - returns: none, y la actualización del diccionario tomado como paramétro 
        con la información de la nueva estación. 
    """

    municipios = ["Medellín","Bello","Itagui","Caldas","La Estrella","Barbosa"]
    nombre_estacion = input("Ingrese el nombre de la estacion, (Es valido cualquier caracter):")
    codigo = generar_codigo(diccionario)
    print("--------------------------")
    indice = 1
    for i in municipios:
        print(i, "("+str(indice)+")" )
        indice += 1
    print("--------------------------")    
    modo = respuesta_numerica("Seleccione uno de los municipios disponibles:\n","123456")
    modo = int(modo)

    diccionario[codigo] = {'nombre_estacion': nombre_estacion, 'municipio': municipios[modo-1] }

def editar_estacion(diccionario):
    
    """
    Sinopsis:
        funcion que usa el diccionario y nos permite editar la información 
        de alguna de las estaciones 


    Entradas y salidas:
        - diccionario: diccionario de estaciones
        - returns: none, y la actualización del diccionario con la nueva 
        información. 
    """

    municipios = ["Medellín","Bello","Itagui","Caldas","La Estrella","Barbosa"]
    print("Los códigos de las estaciones existentes son: ")
    for estacion in diccionario:
        print(estacion, diccionario[estacion]["nombre_estacion"])
    
    validar = False
    while validar != True:
        codigo = input("Escriba el código de la estacion que desea editar:")
        validar = validar_existencia(codigo,diccionario)
        if validar == False:
            print("Ingrese un codigo de estación existente")

    print("El nombre de la estación es:", diccionario[codigo]["nombre_estacion"], ", se le solicitará uno nuevo.")
    nnombre_estacion = input("Ingrese el nombre de la estacion, (Es valido cualquier caracter):")
    diccionario[codigo]["nombre_estacion"] = nnombre_estacion
    print("El municipio actual es:", diccionario[codigo]["municipio"], ", se le solicitará uno nuevo.\n")

    indice = 1
    for i in municipios:
        print(i, "("+str(indice)+")" )
        indice += 1
    print("--------------------------")    
    modo = respuesta_numerica("Seleccione uno de los municipios disponibles:\n","123456")
    modo = int(modo)

def eliminar_estacion(diccionario):

    """
    Sinopsis:
        funcion que usa el diccionario y nos permite editar una estación existente.

    Entradas y salidas:
        - diccionario: diccionario de estaciones
        - returns: none, y la actualización del diccionario. 
    """

    print("Las estaciones existentes son: ")
    for codigo in diccionario:
        print(codigo, diccionario[codigo]["nombre_estacion"])

    validar = False
    while validar != True:
        codigo = input("Escriba el codigo de la estacion que desea eliminar: ")
        validar = validar_existencia(codigo,diccionario)
        if validar == False:
            print("Ingrese un usuario existente")

    del diccionario[codigo]