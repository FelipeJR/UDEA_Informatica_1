def open_file(archivo):

    """
    Sinopsis:
        funcion que permite abrir un archivo dado como argumento y cargarlo en memoria

    Entradas y salidas:
        - archivo: nombre del archivo
        - returns: lineas 
    """

    with open(archivo) as file:
        lineas = file.readlines()
        return lineas

def posLineWhite(archivo):

    """
    Sinopsis:
        funcion que permite calcular la posición de las lineas que está completamente en blanco de un archivo

    Entradas y salidas:
        - archivo: nombre del archivo
        - returns: posLineWhite 
    """

    lectura = open_file(archivo) 
    posLineWhite = []
    pos = 0

    for linea in lectura:
        linea_limpia = linea.rstrip()
        if len(linea_limpia) == 0:
            posLineWhite.append(pos)
        pos += 1

    return posLineWhite

def search_position(linea, separadores):

    """
    Sinopsis:
        funcion que nos permite encontrar las ubicación exacta de los separadores en el string ingresado

    Entradas y salidas:
        - linea: string a evaluar
        - separadores: delimitador a buscar
        - returns: pos: posiciones
    """

    pos = []
    posi = 0
    for caracter in linea:
        posi += 1
        if caracter in separadores:
            pos.append(posi-1)
    return pos



def custom_split(string,separadores):
    
    """
    Sinopsis:
        funcion que nos permite encontrar las ubicación exacta de los separadores en el string ingresado

    Entradas y salidas:
        - linea: string a evaluar
        - separadores: delimitador a buscar
        - returns: pos: posiciones

    """

    pos = search_position(string, separadores)
    list_elementos = []

    # Verificar si el primer elemento no tiene separador antes de él
    if pos[0] != 0:
        list_elementos.append(string[:pos[0]])
        
    # Iterar por los separadores encontrados para obtener los elementos
    for i in range(len(pos) - 1):
        indice0 = pos[i] + 1
        indice1 = pos[i+1]
        list_elementos.append(string[indice0:indice1])
    # Agregar el último elemento
    indice0 = pos[-1] + 1
    list_elementos.append(string[indice0:])
    return list_elementos

def extraer_usuarios(archivo,diccionario):

    """
    Sinopsis:
        funcion que nos extraer los usuarios del archivo registros.txt siguiendo de manera estricto 
        las caracteristicas de la plantilla

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - diccionario: diccionario donde se almacenará la información
        - returns: none: se realizan los cambios durante la ejecución
    """

    file = open_file(archivo)
    indice = 0
    maxIndice = posLineWhite(archivo)[0]
    
    while indice != maxIndice:
        linea = file[indice].rstrip()
        linea = linea[1:-1]
        elementos = custom_split(linea,";")
        diccionario[elementos[0]] = {'usuario': elementos[1], 'contrasena': elementos[2], 'rol': elementos[3]}
        indice += 1

def extraer_departamentos(archivo):

    """
    Sinopsis:
        funcion que nos extrae los departamentos disponibles de la base de datos

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - returns: lista con departamentos
    """

    file = open_file(archivo)
    indice = posLineWhite(archivo)[1]-1
    linea = file[indice].rstrip()
    linea = linea[1:]
    departamentos = custom_split(linea,",")
    return departamentos

def extraer_estaciones(archivo,diccionario):

    """
    Sinopsis:
        funcion que nos extraer las estaciones del diccionario

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - returns: lista con departamentos
    """

    file = open_file(archivo)
    indice = posLineWhite(archivo)[1]+1
    maxIndice = posLineWhite(archivo)[2]-1 
    linea = file[indice].rstrip()

    while indice != maxIndice:
        linea = file[indice].rstrip()
        elementos = custom_split(linea,",")
        diccionario[elementos[0]] = {'nombre_estacion': elementos[1], 'municipio': elementos[2], 'mediciones': [] }
        indice += 1 





def extraer_tipo_variable(archivo):

    """
    Sinopsis:
        funcion que nos extrae los tipos de variables disponibles a analizar de la base de datos

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - returns: lista con los tipos de variable
    """

    file = open_file(archivo)
    indice = posLineWhite(archivo)[2]+1
    linea = file[indice].rstrip()
    lecturas = custom_split(linea,";")
    return lecturas


def extraer_lecturas(archivo):

    """
    Sinopsis:
        funcion que nos extraer las lecturas de la base de datos

    Entradas y salidas:
        - archivo: archivo de dónde se extraerá la información
        - returns: lista con las lecturas
    """

    file = open_file(archivo)
    indice = posLineWhite(archivo)[3]+1
    maxIndice = len(file)
    lista = []

    while indice != maxIndice:
        linea = file[indice].rstrip()
        lista.append(custom_split(linea,";"))
        indice += 1
    return lista











"""
usuarios = {}
estaciones = {}
departamentos = extraer_departamentos("registros.txt")

extraer_usuarios("registros.txt",usuarios)
extraer_estaciones("registros.txt",estaciones)

print("DIC de USUARIOS")
print(usuarios)
print("DIC de ESTACIONES")
print(estaciones)
print("LIST de DEPARTAMENTOS")
print(departamentos)
"""





