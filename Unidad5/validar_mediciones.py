def respuesta_numerica(string,digitos_validos_str):

    """
    Sinopsis:
        Función que permite validar si el usuario ingresa un número de un 
        conjunto especifico dado como argumento


    Entradas y salidas:
        - digitos_validos_str: conjunto de números validos en str
        - string: texto que se mostrará por pantalla al ejecutar el input
        - returns: num
    """

    num = ""
    while num not in digitos_validos_str or len(num) != 1 or len(num) != 1:
        num = input(string)
        if num not in digitos_validos_str or len(num) != 1:
            print("La opción no es valida")
    return num

def validacion_rango(valor, minimo, maximo):

    """
    Sinopsis:
        Función que permite validar si un número está en un rango dado


    Entradas y salidas:
        - valor: valor para hacer la comprobación
        - minimo: valor minimo del rango
        - maximo: valor maximo del rango
        - returns: True o False dependiendo de si es valido o no
    """

    if valor >= minimo and valor <= maximo or valor == "ND":
        return True
    else:
        return False


def input_valor_validacion(string):
    
    """
    Sinopsis:
        Función que permite validar si un string se puede castear a un número valido
        , si el se toma como argumento ND o ND la funcion retorna ND


    Entradas y salidas:
        - string: valor para hacer la comprobación
        - returns: numero en entero o ND en ese caso particular
    """

    caracteres = "0123456789NDnd"
    contCaracteresInvalidos = 1

    while contCaracteresInvalidos != 0:
        contCaracteresInvalidos = 0
        p = input(string, )
        for caracter in p:
            if caracter not in caracteres:
                contCaracteresInvalidos += 1
                print("Ingrese un valor valido")
                break
        if p.upper() == "ND":
            return "ND" 
    return int(p) 


def input_variable_medicion(tipo):

    """
    Sinopsis:
        funcion que permite solicitar al usuario que ingrese cualquiera de los 4 tipos 
        de variables. 


    Entradas y salidas:
        - tipo: tipo de variable denotado con un numero:
        0.PM25
        1.PM30
        2.Temperatura
        3.Humedad
        - returns: valor de la variable
    """

    estado = True
    if tipo == 0:
        while estado:
            var = input_valor_validacion("Ingrese la medición de PM10 en ug/m3:")   
            if type(var) == str:
                return -999
            if validacion_rango(var,0.0,100.0):
                estado = False
            if validacion_rango(var,0.0,100.0) == False:
                print("Ingrese un valor valido")

    elif tipo == 1:
        while estado:
            var = input_valor_validacion("Ingrese la medición de PM25 en ug/m3:")
            if type(var) == str:
                return -999
            if validacion_rango(var,0.0,200.0):
                estado = False
            if validacion_rango(var,0.0,200.0) == False:
                print("Ingrese un valor valido")

    elif tipo == 2:
        while estado:
            var = input_valor_validacion("Ingrese la medición de temperatura en °C:")
            if type(var) == str:
                return -999
            if validacion_rango(var,-20.0,50.0):
                estado = False
            if validacion_rango(var,-20,50.0) == False:
                print("Ingrese un valor valido")

    elif tipo == 3:
        while estado:
            var = input_valor_validacion("Ingrese la medición de humedad en %:")
            if type(var) == str:
                return -999
            if validacion_rango(var,0.0,100.0):
                estado = False
            if validacion_rango(var,0.0,100.0) == False:
                print("Ingrese un valor valido")

    return var





