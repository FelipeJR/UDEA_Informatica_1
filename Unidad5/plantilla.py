"""
# Diccionario con los datos a escribir
datos = {'nombre': 'Juan', 'edad': 25}

# Plantilla para el archivo de texto
plantilla = "Nombre: {}\nEdad: {}\n"

# Abrir el archivo en modo escritura
with open('datos.txt', 'w') as archivo:
    # Escribir los datos en el archivo usando la plantilla
    archivo.write(plantilla.format(datos['nombre'], datos['edad']))
"""
diccionario = {
'1035972061': {
'usuario': 'ufelipejr',
'contrasena': 'fjrsh',
'rol': 'Administrador'
},
'1033178195': {
'usuario': 'dragon777',
'contrasena': 'fjrsh',
'rol': 'Operador'
},
'1212121212': {
'usuario': 'mono',
'contrasena': 'ijidddd',
'rol': 'Operador'
}

}

# Lista para almacenar las líneas
lineas = []

# Recorrer el diccionario y construir las líneas
for clave, valor in diccionario.items():
    # Construir la línea con el formato requerido
    linea = f"<{clave};{valor['usuario']};{valor['contrasena']};{valor['rol']}>"
    # Agregar la línea a la lista
    lineas.append(linea)

# Escribir las líneas en un archivo de texto
with open("datos.txt", "w") as archivo:
    for linea in lineas:
        archivo.write(f"{linea}\n")
