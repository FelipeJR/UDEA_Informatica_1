from utilidadesA import *
from funcionesPrincipales import *
from gestionUsuariosEstaciones import *

usuarios = {}
estaciones = {
    '1': {
        'nombre_estacion': 'Universidad San Buenaventura',
        'municipio': 'Medellin',
        'mediciones': {
            '2019-07-01 00:00:00': {
                'PM10': '3.5',
                'PM25': '6.2',
                'Temperatura': '27.0',
                'Humedad': '34.0'
            },
            '2019-07-01 00:00:10': {
                'PM10': '3.6',
                'PM25': '6.2',
                'Temperatura': '27.0',
                'Humedad': '34.0'
            },
            '2019-07-01 00:00:11': {
                'PM10': '3.7',
                'PM25': '6.2',
                'Temperatura': '27.0',
                'Humedad': '34.0'
            },
            '2019-07-01 00:00:12': {
                'PM10': '3.8',
                'PM25': '6.2',
                'Temperatura': '27.0',
                'Humedad': '34.0'
            },
            '2019-07-01 00:00:20': {
                'PM10': '3.9',
                'PM25': '6.3',
                'Temperatura': '27.1',
                'Humedad': '34.1'
            },
            '2019-07-01 00:00:30': {
                'PM10': '4.0',
                'PM25': '6.3',
                'Temperatura': '27.1',
                'Humedad': '34.1'
            },
            '2019-07-01 00:00:40': {
                'PM10': '4.1',
                'PM25': '6.3',
                'Temperatura': '27.1',
                'Humedad': '34.1'
            },
            '2019-07-01 00:00:50': {
                'PM10': '4.2',
                'PM25': '6.3',
                'Temperatura': '27.1',
                'Humedad': '34.1'
            },
            '2019-07-01 00:01:00': {
                'PM10': '4.3',
                'PM25': '6.3',
                'Temperatura': '27.1',
                'Humedad': '34.1'
            },
            '2019-07-01 00:01:10': {
                'PM10': '4.4',
                'PM25': '6.3',
                'Temperatura': '27.1',
                'Humedad': '34.1'
            },
            '2019-07-01 00:01:20': {
                'PM10': '4.5',
                'PM25': '6.3',
                'Temperatura': '27.1',
                'Humedad': '34.1'
            }
        }
    }
}


def mediciones_lista(diccionario, codigo):
    lista = []
    
    for i in diccionario[codigo]["mediciones"]:
        medicion = [
            diccionario[codigo]["nombre_estacion"],
            codigo,
            i,
            diccionario[codigo]["mediciones"][i]["PM10"],
            diccionario[codigo]["mediciones"][i]["PM25"],
            diccionario[codigo]["mediciones"][i]["Temperatura"],
            diccionario[codigo]["mediciones"][i]["Humedad"]
        ]
        lista.append(medicion)
    
    return lista



def VisualizarEstadisticas(diccionario,estaciones,modo):

    """
    Función que permite visualizar las estadisticas de cierta variable

    Entradas y salidas
        - estaciones (dict): Diccionario que contiene la información de las estaciones

    Returns:
        None. Imprime la información solicitada por el usuario

    """

    clean_screen()
    print("Seleccione el código de una de las siguientes estaciones:\n ")
    for i in estaciones:
        estacion = estaciones[i]["nombre_estacion"]
        print(i,estacion)
    print("--------------------------")

    nDis = opciones_validas(estaciones)
    estacion = respuesta_numerica("Seleccione la Estacion: ",nDis)
    clean_screen()
    var = escoger_variables()

    opciones = \
           "Escoja el tipo de Analisis\n"\
           "1. Últimos 7 días\n" \
           "2. Últimos 30 días\n" \
           "3. Fecha Especifica\n" \
           "--------------------------\n"
    clean_screen()
    modoEstadistica = respuesta_numerica(opciones, "123")
    

    if modoEstadistica == "1":
        print("1")
    elif modoEstadistica == "2":
        print("2")
    elif modoEstadistica == "3":
        print("3")

    opciones = \
           "¿Qué desea realizar?\n"\
           "1. Retroceder\n" \
           "2. Salir\n" \
           "--------------------------\n"
    clean_screen()
    modoA = respuesta_numerica(opciones, "12")

    if modoA == "1":
        clean_screen()
        inicio_de_sesion(usuarios,estaciones)
        
    elif modo == "2":
        clean_screen()
        exit()

def MNpromedioDias(diccionario, variable, codigo_estacion, opcion):
    """
    Función que permite encontrar el valor promedio, mínimo y máximo de cierta variable en los
    últimos 7 o 30 días de una estación específica.

    Args:
        - diccionario (dict): Diccionario que contiene la información de las estaciones.
        - variable (str): Variable que se desea consultar.
        - codigo_estacion (str): Código de la estación que se desea consultar.
        - opcion (int): Opción que indica si se desea calcular los valores de los últimos 7 o 30 días.

    Returns:
        None. Imprime los valores promedio, máximo y mínimo de la variable en los últimos 7 o 30 días.
    """

    if opcion == 1:
        opcion = -7
    else:
        opcion = -30

    lista = mediciones_lista(diccionario, codigo_estacion)
    lista_filtrada = [elemento for elemento in lista if elemento[1] == variable]

    valores = [float(elemento[3]) for elemento in lista_filtrada[opcion:]]
    promedio = sum(valores) / len(valores)
    minimo = min(valores)
    maximo = max(valores)

    print(f"Variable: {variable}")
    print(f"Estación: {codigo_estacion}")
    print(f"Últimos {abs(opcion)} días:")
    print(f"Promedio: {promedio}")
    print(f"Mínimo: {minimo}")
    print(f"Máximo: {maximo}")



def MNpromedioDiasX(departamentos,estaciones, opcion):
    
    """
    Función que permite encontrar el valor promedio, mínimo y máximo de cierta variable en los
    últimos 7 o 30 días de una estación específica.

    Args:
        - diccionario (dict): Diccionario que contiene la información de las estaciones.
        - variable (str): Variable que se desea consultar.
        - codigo_estacion (str): Código de la estación que se desea consultar.
        - opcion (int): Opción que indica si se desea calcular los valores de los últimos 7 o 30 días.

    Returns:
        None. Imprime los valores promedio, máximo y mínimo de la variable en los últimos 7 o 30 días.

    """











tabla = [
    ['Estación A', 30, 15, 22.5, '2023-05-01', '12:00'],
    ['Estación B', 28, 18, 23.6, '2023-05-01', '12:00'],
    ['Estación C', 32, 20, 26.8, '2023-05-01', '12:00'],
    ['Estación D', 29, 16, 21.2, '2023-05-01', '12:00']
]
ancho = [15, 13, 12, 10, 12, 8]
encabezado = ['Estación', 'Valor Máximo', 'Valor Mínimo', 'Promedio', 'Fecha', 'Hora']

imprimir_tabla(tabla, ancho, encabezado)






    
    


























