from utilidadesA import *
from funcionesPrincipales import *
from gestionUsuariosEstaciones import *

#estaciones = {}
#extraer_estaciones("registros.txt",estaciones)

estaciones = {
    '1': {
        'nombre_estacion': 'Universidad San Buenaventura',
        'municipio': 'Medellin',
        'mediciones': {
            '2019-07-01 00:00:00': {
                'PM10': '3.5',
                'PM25': '6.2',
                'Temperatura': '27.0',
                'Humedad': '34.0'
            },
            '2019-07-01 00:00:10': {
                'PM10': '3.6',
                'PM25': '6.2',
                'Temperatura': '27.0',
                'Humedad': '34.0'
            },
            '2019-07-01 00:00:11': {
                'PM10': '3.7',
                'PM25': '6.2',
                'Temperatura': '27.0',
                'Humedad': '34.0'
            },
            '2019-07-01 00:00:12': {
                'PM10': '3.8',
                'PM25': '6.2',
                'Temperatura': '27.0',
                'Humedad': '34.0'
            },
            '2019-07-01 00:00:20': {
                'PM10': '3.9',
                'PM25': '6.3',
                'Temperatura': '27.1',
                'Humedad': '34.1'
            },
            '2019-07-01 00:00:30': {
                'PM10': '4.0',
                'PM25': '6.3',
                'Temperatura': '27.1',
                'Humedad': '34.1'
            },
            '2019-07-01 00:00:40': {
                'PM10': '4.1',
                'PM25': '6.3',
                'Temperatura': '27.1',
                'Humedad': '34.1'
            },
            '2019-07-01 00:00:50': {
                'PM10': '4.2',
                'PM25': '6.3',
                'Temperatura': '27.1',
                'Humedad': '34.1'
            },
            '2019-07-01 00:01:00': {
                'PM10': '4.3',
                'PM25': '6.3',
                'Temperatura': '27.1',
                'Humedad': '34.1'
            },
            '2019-07-01 00:01:10': {
                'PM10': '4.4',
                'PM25': '6.3',
                'Temperatura': '27.1',
                'Humedad': '34.1'
            },
            '2019-07-01 00:01:20': {
                'PM10': '4.5',
                'PM25': '6.3',
                'Temperatura': '27.1',
                'Humedad': '34.1'
            }
        }
    }
}




def MNpromedioDiasX(estaciones, opcion):
    clean_screen()
    departamentos = escoger_municipio()
    listM = []
    listEstaciones = []
    
    if departamentos[0]:
        listM.append("Medellin")
    if departamentos[1]:
        listM.append("Bello")
    if departamentos[2]:
        listM.append("Itagui")
    if departamentos[3]:
        listM.append("Caldas")
    if departamentos[4]:
        listM.append("La Estrella")
    if departamentos[5]:
        listM.append("Barbosa")

    if opcion == 1:
        opcion = -7
    else:
        opcion = -30

    for i in estaciones:
        mun = estaciones[i]["municipio"]
        if mun in listM:
            listEstaciones.append(estaciones[i])
    
    # Tabla PM10
    listaPM10 = []
    for estacion in listEstaciones[opcion:]:
        mediciones_estacion = estacion['mediciones']
        for fecha, mediciones in mediciones_estacion.items():
            medicion_PM10 = mediciones.get('PM10')
            if medicion_PM10 is not None:
                listaPM10.append([estacion['nombre_estacion'], fecha, medicion_PM10])

    Vmin = 1000000000000
    estacionMin = ""
    horaMedMin = ""

    vMax = 0
    estacionMax = ""
    horaMedMax = ""

    promedio = 0
    suma = 0
    contador = 0

    for i in listaPM10:
        medicion = float(i[2])  
        if medicion != -999:
            if medicion < Vmin:
                Vmin = medicion
                estacionMin = i[0]
                horaMedMin = i[1]
            if medicion > vMax:
                vMax = medicion
                estacionMax = i[0]
                horaMedMax = i[1]
            suma += medicion
            contador += 1

    if contador > 0:
        promedio = suma / contador

    TablaPM10 = []
    encabezado = ["Variables","Valores PM10"]
    ancho = [40, 40]
    TablaPM10.append(["Estación con mínima medición", estacionMin])
    TablaPM10.append(["Mínima Medición", Vmin])
    TablaPM10.append(["Fecha y Hora de Medición", horaMedMin])
    TablaPM10.append(["Estación con máxima medición", estacionMax])
    TablaPM10.append(["Máxima Medición", vMax])
    TablaPM10.append(["Fecha y Hora de Medición", horaMedMax])
    TablaPM10.append(["Promedio de los municipios", promedio])
    imprimir_tabla(TablaPM10, ancho, encabezado)










MNpromedioDiasX(estaciones,1)



