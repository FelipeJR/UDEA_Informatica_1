import math

def init_var():

    """
    Sinopsis:
        funcion encargada de globalizar todas las variables que son utilizadas 
        en el resto de funciones. 

    Entradas: none
    Salidas: none
        
    """

    global Vc,radius,length,eccentricity,constGas,temperature #Se globalizan las variables
    
    Vc = 825  #Cilindraje del motor en cm^3
    constGas = 286.9 #Constante del gas ideal
    radius = 5.24  #Radio del cigueñal en cm
    temperature = 278.15 #kelvin

    length = request_value("Ingrese la longitud de la biela en cm, tenga en cuenta que debe ser un valor mayor al de excentricidad:") #input_entrada de longitud
    eccentricity = request_value("Ingrese excentricidad entre el centro del pistón y el centro del cigüeñal:") #input_entrada de excentricidad  
    
    #temperature = request_value("Ingrese la temperatura en kelvin: ") #input_entrada de temperatura

    # length = 9.53  # cm
    # eccentricity = 2.9   # cm 

def request_value(string):
    """
    Sinopsis:
        Solicita al usuario un valor numérico y lo devuelve como un número de punto flotante y 
        si el valor númerico no es valido, solicita uno nuevamente.

    Entradas: String
    Salidas: Float

    Excepciones:
    - Si el usuario ingresa un valor no numérico, se captura la excepción ValueError y se muestra un mensaje de error.
    """
    while True:
        try:
            request_value = float(input(string))
            return request_value
        except ValueError:
            print("Ingrese un valor válido.")

def calculate_position(angle):
    """
    Sinopsis:
        Calcula la posición vertical de un motor de combustión, de acuerdo al angulo dado.

    Entradas:
        int(angle):El algulo hasta donde se evaluara la pisición.
    Salidas:
        La posición del motor en el angulo dado.
    """
    return radius * (math.sin(angle) + (length / radius) * math.sqrt(1 - ((eccentricity - radius * math.cos(angle)) / length) ** 2))

def calculate_velocity(angle, omega):
     
    """
     Sinopsis:
        Calcula la velocidad de un motor de combustión en función de un ángulo y una velocidad dada.

    Parámetros:
        - angle: El ángulo.
        - omega: La velocidad angular.

    Retorna:
        La velocidad del motor de combustion.
    """
    return (calculate_position(angle + 0.1) - calculate_position(angle)) / ((0.1 / 360) * 2 * math.pi / omega)

def calculate_acceleration(angle, omega):
    """
    Sipnosis:
        Calcula la aceleración de un motor de conbustión en función de un ángulo y una velocidad angular dados.

    Parámetros:
        - angle: El ángulo dado.
        - omega: La velocidad angular.

    Retorna:
        - La aceleración del motor de combustion.
    """
    return (calculate_velocity(angle + 0.1, omega) - calculate_velocity(angle, omega)) / ((0.1 / 360) * 2 * math.pi / omega)

def calculate_volume(angle):
    """
    Sipnosis:
        -Calcula el volumen de un motor de combuistión en función de un ángulo dado.

    Parámetros:
        - angle: El ángulo dado.

    Retorna:
        - El volumen del motor de combustión.
    """
    return Vc + math.pi * (radius ** 2) * (eccentricity - radius * math.cos(angle)) + (length + eccentricity * math.sin(angle)) * math.sqrt(length ** 2 - (eccentricity - radius * math.cos(angle)) ** 2)

def calculate_pressure(angle,volume):
    """
    Sinopsis:
        -Calcula la presión de un gas en función de un ángulo y un volumen dado.

    Parámetros:
    - angle: El ángulo dadp.
    - volume: El volumen de la camara del motor en unidades de centímetros cúbicos.

    Retorna:
        -La preción del gas del motor de combustión.
    """
    volume = calculate_volume(angle)
    pressure = constGas*temperature/(volume/1000000)
    return pressure



