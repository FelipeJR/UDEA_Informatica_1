Algoritmo equation_final
	// Felipe Jimènez Ramírez
	// Juan Esteban López Castrillòn
	// Grupo 4
	Escribir 'Ingrese coeficiente cuadrático: ' // Se le indica al usuario que ingrese el coeficiente cuadratico ^2
	Leer a // a: almacena el valor del coeficiente cuadratico
	Escribir 'Ingrese coeficiente líneal: ' // Se le indica al usuario que ingrese el coeficiente lineal ^1
	Leer b // b: almacena el valor del coeficiente cuadratico
	Escribir 'Termino independiente: ' // Se le indica al usuario que ingrese el termino independiente
	Leer c // c: almacena el termino independiente
	discriminante <- (((b^2)-(4*a*c))^(1/2)) // discriminante: se sustituyen los valores y se calcula el valor de la discriminante
	raiz1 <- ((-b)+discriminante)/(2*a) // raiz1: Encuentra el valor de la raiz 1
	raiz2 <- ((-b)-discriminante)/(2*a) // raiz2: Encuentra el valor de la raiz 2
	Si discriminante<0 Entonces // Se evalúa si el discriminante es igual a 0
		Escribir 'El resultado serán dos raices imaginarias y conjugadas' // Se imprime el tipo de solución
		Escribir 'X1: ',raiz1 // Se imprime la raiz1
		Escribir 'X2: ',raiz2 // Se imprime la raiz2
	SiNo
		Si discriminante==0 Entonces // Se evalúa si el discriminante es mayor a 0
			Escribir 'El resultado serán 2 raices reales y repetidas' // Se imprime el tipo de solución - Se imprime la raiz1
			Escribir 'X1: ',raiz1
			Escribir 'X2: ',raiz2 // Se imprime la raiz2
		SiNo
			Si discriminante>0 Entonces // Se evalúa si el discriminante es menor a 0
				Escribir 'El resultado serán dos raices reales distintas' // Se imprime el tipo de solución
				Escribir 'X1: ',raiz1 // Se imprime la raiz1
				Escribir 'X2: ',raiz2 // Se imprime la raiz2
			SiNo
				Escribir "El resultado serán dos raices complejas conjugadas" // Caso extremo en dado caso de que ninguna condicion se cumpla, (generalmente se cumple alguna)
				Escribir "X1: ", raiz1
				Escribir "X2: ", raiz2
			FinSi
		FinSi
	FinSi
FinAlgoritmo
