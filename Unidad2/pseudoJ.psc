Algoritmo MCD_FInal
	// Felipe Jiménez Ramírez
	// Juan Esteban López Castrillón
	// Grupo 4
	Escribir 'Ingrese el primer número: ' // Solicita el número 1
	Leer number1 // number1: almacena el valor ingresado por el usuario
	Escribir 'Ingrese el segundo número: ' // Solicita el número 2
	Leer number2 // number2: almacena el segundo valor ingresado por el usuario
	Si number1<number2 Entonces // Condicional para invertir los valores en dado caso de que el número 1 sea menor que el número 2
		aux <- number1 // Aux: El proceso aux toma el valor de number1
		number1 <- number2 // Number2 toma el valor de number1
		number2 <- aux // Number2 toma el valor de aux
	FinSi
	Mientras number2<>0 Hacer // Ciclo: mientras que el valor de number 2 será diferente de cero se ejecutará
		residuo <- number1 MOD number2 // residuo: cálculo de residuo entre número1 y número2
		number1 <- number2 // Número1 toma el valor de número2
		number2 <- residuo // Número2 toma el valor de residuo
	FinMientras
	Escribir 'El MCD es: ',number1 // Se imprime el valor de number1, que a su vez es el MCD.
FinAlgoritmo
