Algoritmo streaming
	// Felipe Jiménez Ramírez
	// Juan Esteban López Castrillòn
	// Grupo 4
    Escribir "Ingrese hora de inicio : " //Se solicita que se ingrese la hora de inicio en formato HHMMSS
    Leer HoraInicio //HoraInicio: almacena la hora de inicio
    Escribir "Ingrese hora de finalizacion : " //Se solicita que se ingrese la hora de finalización en formato HHMMSS
    Leer HoraFinal //HoraFinal: almacena la hora de finalizacion
	
    SegundosTotales1 <- ((trunc(HoraInicio/10000))*3600)+((trunc(HoraInicio/100)MOD 100)*60) + (trunc(HoraInicio MOD 100)) //Calculamos los segundos totales, obteniendo el número de horas, minutos y segundos por separado
    SegundosTotales2 <- ((trunc(HoraFinal/10000))*3600)+((trunc(HoraFinal/100)MOD 100)*60) + (trunc(HoraFinal MOD 100)) //Calculamos los segundos totales, obteniendo el número de horas, minutos y segundos por separado
    diferencia <- abs(SegundosTotales2 - SegundosTotales1) //Obtenemos la cantidad de tiempo(segundos) transcurrido entre el SegundosTotales2 y SegundosTotales1 
	CostoTotal <- diferencia*2 //Calculamos el costo total multiplicando por dos
	Escribir "El costo total es: ", CostoTotal //Imprimos el CostoTotal
	
FinAlgoritmo