Algoritmo rad_to_grade
	// Felipe Jimènez Ramìrez
	// Juan Esteban Lòpez Castrillòn 
	// Grupo 4
	Escribir "Ingrese el valor de PI radianes :" //Solicita al usuario que ingrese el valor que será convertido a radianes
	Leer rad //rad: almacena el valor ingresado por el usuario
	conver <- (rad*180)/PI //conver: cálculo para la conversión de radianes a grados
	Escribir "El valor en grados es: ", conver //Se imprime el valor convertido a grados
FinAlgoritmo
