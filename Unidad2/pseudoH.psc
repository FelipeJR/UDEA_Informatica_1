Algoritmo machine_completo
	// Felipe Jimènez Ramírez
	// Juan Esteban López Castrillòn
	// Grupo 4
	Escribir 'Ingrese la devuelta: ' //Se le solicita que se ingrese la devuelta
	Leer devuelta //devuelta: almacena el valor de la devuelta previamente ingresado
	dinero_restante <- devuelta //dinero_restante: inicialmente toma el valor de devuelta, pero este cambiara durante la ejecucion
	monedas_1000 <- trunc(dinero_restante/1000) //monedas_1000: Se calcula por medio de una division entera la cantidad de monedas de 1000 con ayuda del valor que tiene dinero_restante
	dinero_restante <- dinero_restante MOD 1000 //Se calcula el dinero restante por medio del módulo
	monedas_500 <- trunc(dinero_restante/500) //monedas_500: Se calcula por medio de una division entera la cantidad de monedas de 500 con ayuda del valor que tiene dinero_restante
	dinero_restante <- dinero_restante MOD 500 //Se calcula el dinero restante por medio del módulo
	monedas_200 <- trunc(dinero_restante/200) //monedas_200: Se calcula por medio de una division entera la cantidad de monedas de 200 con ayuda del valor que tiene dinero_restante
	dinero_restante <- dinero_restante MOD 200 //Se calcula el dinero restante por medio del módulo
	monedas_100 <- trunc(dinero_restante/100) //monedas_100: Se calcula por medio de una division entera la cantidad de monedas de 100 con ayuda del valor que tiene dinero_restante
	dinero_restante <- dinero_restante MOD 100 //Se calcula el dinero restante por medio del módulo
	monedas_50 <- trunc(dinero_restante/50) //monedas_50: Se calcula por medio de una division entera la cantidad de monedas de 50 con ayuda del valor que tiene dinero_restante
	dinero_restante <- dinero_restante MOD 50 //Se calcula el dinero restante por medio del módulo
	
	Si dinero_restante>0 Entonces //Si aún hay dinero restante, es decir que este es mayor a 0
		Escribir 'Se deben devolver ',monedas_1000,' de mil, ',monedas_500,' de quinientos, ',monedas_200,' de docientos, ',monedas_100,' de cien, ',monedas_50,' de cincuenta,',' y quedan por entregar ',dinero_restante //Se imprime la cantidad de monedas de cada tipo incluyendo el dinero restante
	SiNo
		Escribir 'Se deben devolver ',monedas_1000,' de mil, ',monedas_500,' de quinientos, ',monedas_200,' de docientos, ',monedas_100,' de cien, y ',monedas_50,' de cincuenta' //Si no hay dinero restante se imprime la cantidad de monedas de cada tipo
	FinSi
FinAlgoritmo
