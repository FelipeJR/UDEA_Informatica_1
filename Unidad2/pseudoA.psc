Algoritmo algoritmo1
	// Felipe Jimènez Ramírez
	// Juan Esteban López Castrillòn
	// Grupo 4
	Escribir 'Ingrese uno de los lados que se repite: ' // l1: valor del lado largo
	Leer l1 //"l1 almacena el valor del lado que se repite"
	Escribir 'Ingrese el lado que no se repite: '// l2: valor del lado corto
	Leer l2 //"l2 almacena el valor del lado que no se repite"
	perimetro <- (l1*2)+l2 // El proceso altura convierte el triángulo en dos triángulos rectangulos y en base a ello calcula el valor del cateto que es igual a la altura
	altura <- ((l1^2)-(l2/2)^2)^(1/2) // El proceso perimetro, multiplica el lado de mayor longitud por 2 y lo suma al lado de menor longitud.
	area <- (l2*altura)/2 // El proceso area usa el valor de altura para calcular dicho valor
	Escribir 'El perimetro: ',perimetro //Imprime en la sálida el valor de perimetro
	Escribir 'La altura: ',altura //Imprime en la sálida el valor de altura
	Escribir 'El area es: ',area //Imprime en la sálida el valor de area
FinAlgoritmo
