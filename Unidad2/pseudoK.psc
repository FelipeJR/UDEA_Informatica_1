Algoritmo triangulo_variable
	// Felipe Jimènez Ramírez
	// Juan Esteban López Castrillòn
	// Grupo 4
	
    Escribir 'Escribe el tamaño del triángulo: ' //Se le solicita al usuario que ingrese el tamaño del triangulo
    Leer n //n: almacena el valor que determina el tamaño del triángulo
    i <- 1 //i: iterador inicializado en 1
	
    mientras i <= n hacer //Se define un bucle que se ejecutará mientras el iterador(i) sea menor que el número ingresado
        j <- 1			//Se inicializa iterador j con el valor 1
        mientras j <= i hacer //Se define otro bucle que se ejecutará mientras j sea menor o igual que el valor de i
            Escribir '*' Sin Saltar //Se imprime el *
            j <- j + 1 //Se incrementa en 1 el valor de j
        FinMientras //Finaliza el bucle
        Escribir '' //Escribe un espacio vácio para hacer un salto de línea
		
        //si i == n entonces
          //  r <- n - 1
            //mientras r >= 1 hacer
              //  s <- 1
                //mientras s <= r-1 hacer
                  //  Escribir '*' Sin Saltar
                    //s <- s + 1
                //FinMientras
                //Escribir ''
                //r <- r - 2
            //FinMientras
        //FinSi
        i <- i + 2
    FinMientras
FinAlgoritmo
