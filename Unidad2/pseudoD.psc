Algoritmo calculo_bicicleta
	//Felipe Jiménez Ramírez
	//Juan Esteban López Castrillón 
	//Grupo 4
	
	Escribir 'Ingrese el número de horas: ' //Se le solicita al usuario que ingrese el número de horas
	Leer hours //hours: almacena el valor de horas que se le solicitó al usuario
	Escribir 'Ingrese el número de minutos: ' //Se le solicita al usuario que ingrese el número de minutos
	Leer minutes //minutes: almacena el valor de minutos que se le solicitó al usuario 
	total <- (hours*60)+(minutes) // total: Horas se multiplica por 60 y se le suma el valor de minutos para así obtener los minutos totales
	no_price <- total*7  //no_price: se define el cobro de 7 pesos en una multiplicación
	increase1 <- total*50 //increase1: se define el cobro de 50 pesos en una multiplicación 
	permanent <- 96000 //permanent: se define el cobro fijo de 960000
	Si total>=101 Entonces //Condicional para evaluar si se sobrepaso 1h con 40m 
		Si total>1440 Entonces //Condicinal para evaluar si se superan las 24 horas 
			Escribir 'Usted usó la bicicleta por ',hours,' horas con ',minutes,' minutos',' por lo tanto serán cobrados ',permanent,' pesos' //Se define la salida en pantalla para cobro fijo
		SiNo
			Escribir 'Usted usó la bicicleta por ',hours,' horas con ',minutes,' minutos',' por lo tanto serán cobrados ',increase1,' pesos' //Se define la salida en pantalla para el cobro de 50 por minuto
		FinSi
	SiNo
		Escribir 'Usted usó la bicicleta por ',hours,' horas con ',minutes,' minutos',' por lo tanto serán cobrados ',no_price,' pesos' //Se define la salida en pantalla para el cobro de 7 por minuto
	FinSi
FinAlgoritmo
