Algoritmo patron2
	// Felipe Jiménez Ramírez
	// Juan Esteban López Castrillón
	// Grupo 4
	Escribir 'Ingrese un número impar: ' // Se le pide al usuario que ingrese un número impar 
	Leer num // num: almacena el valor ingresado por el usuario
	numLineas <- trunc(num/2) // numLineas: calcula el número de filas que tendrá la figura sin contar la inicial por medio de una división entera.
	saltos <- 0 // saltos: inicializamos un contador en cero para contar el número de saltos
	i <- 0 // i: inicializamos un iterador en cero
	Mientras i<=numLineas Hacer // Inicia un ciclo mientras la variable "i" sea menor o igual al número de filas .
		linea <- '' // linea: inicializamos la variable línea como un caracter vacío
		j <- 1 // j: inicializamos un iterador en 1
		Mientras j<=num Hacer //Inicia un ciclo mientras la variable "j" sea menor o igual al número ingresado por el usuario
			linea <- linea+'+' //linea: agrega y concatena el caracter (+) al proceso linea
			j <- j+1			//icrementa en 1 el contador
		FinMientras		//Final del ciclo
		espacios <- ''		//Espacios: inicializa la variable como un caracter vacío
		j <- 1				//Cambia el valor de j a 1
		Mientras j<=saltos Hacer //Inicia un ciclo mientras la variable "j" sea menor o igual al número de saltos (almacenado en la variable "saltos").
			espacios <- espacios+' ' //Espacio: concatena el caracter espacio a la variable "espacios"
			j <- j+1 //Aumenta en un el iterador j
		FinMientras //Finaliza el ciclo
		Escribir linea+espacios+linea //Muestra en la pantalla la concatenación de las variables "linea", "espacios" y "linea"
		saltos <- saltos+4 //Incrementa en 4 el valor del proceso saltos
		num <- num-2 //Decrementa en dos el valor de la variable num
		i <- i+1 //Incrementa en 1 el valor de i
	FinMientras
FinAlgoritmo
