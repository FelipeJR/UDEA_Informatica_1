from modulos_generales import *

def initEstructura():
    global listCourses
    listCourses = extraer_cursos("database.txt")
    global datos
    datos = definir_datos("database.txt")


def sortExtra(listaMaterias):
    listaAux = []
    for i in listaMaterias:
        listaAux.append(i.lstrip())
    return listaAux

def extraer_cursos(archivo):
    posWhite = posLineWhite(archivo)
    linMaterias = open_file(archivo)
    ListMaterias = sortExtra(custom_split(linMaterias[posWhite[0]-1].rstrip(),","))
    return ListMaterias

def extraer_documentos(archivo):
    posWhite = posLineWhite(archivo)
    linDocumentos = open_file(archivo)
    documentos = linDocumentos[posWhite[0]+1:posWhite[1]]
    ListDocumentos = custom_split(documentos[-1].rstrip()," ")
    return ListDocumentos

def extraer_notas(archivo):
    posWhite = posLineWhite(archivo)
    linNotas = open_file(archivo)
    notas = linNotas[posWhite[1]+1:len(linNotas)+1]
    
    listNotas = []
    for i in notas:
        listaProvi = []
        ListAux = custom_split(i.rstrip()," ")
        for j in ListAux:
            listaProvi.append(float(j))
        listNotas.append(listaProvi)
    return listNotas

def mostrar_estudiantes():
    print(datos)

def mostrar_cursos():
    print(listCourses)
    
def delete_course():

    def eliminar_notas_curso(curso):
        pos = 0
        for i in range(len(listCourses)):
            if listCourses[i] == curso:
                pos = i
                break

        notasAux = []

        for i in datos[1]:
            listAux = []
            i[pos] = ""
            j = 0
            while len(listAux) != len(datos[1][0])-1:
                if i[j] == "":
                    pass
                else:
                    listAux.append(i[j])
                j += 1

            notasAux.append(listAux)
        datos[1] = notasAux

    clean_screen()
    cont = 0
    print("Seleccione número correspondiente al curso que desea eliminar: ")
    listAux = []
    listValidos = []

    indice = 1
    for i in listCourses:
        cont += 1
        print(f"{indice}. {i}")
        listValidos.append(str(indice))
        indice += 1

    opcion = int(respuesta_numerica("Seleccione el código del curso que desea eliminar: ",listValidos))
    print("Recuerde que no podrá retroceder")
    validar = respuesta_numerica("¿Desea continuar (1.) si o (2.) no?: ",["1","2"])
    
    if validar == "1":

        print("Ha sido eliminado correctamente")
        sleep(1)
        listAux = []
        cursoSel = listCourses[opcion-1]
        listCourses[opcion-1] = ""
        
        i = 0

        while len(listAux) != len(listCourses) -1:
            cont += 1
            if listCourses[i] != "":
                listAux.append(listCourses[i])
            i += 1
        
        custom_clear(listCourses)
        for i in listAux:
            cont += 1
            listCourses.append(i)

        eliminar_notas_curso(cursoSel)
        gestion_cursos()

    elif validar == "2":
        gestion_cursos()

def solicitudNota():
    vmin = 0
    vmax = 5
    excepcion1 = -1
    excepcion2 = -2
    nota = ""

    while True:
        nota = input("Ingrese la nota: ")
        try:
            nota = int(nota)
            if nota >= vmin and nota <= vmax:
                return float(nota)  
            elif nota == excepcion1 or nota == excepcion2:
                return float(nota)  
            else:
                print("La nota ingresada no es válida. Inténtelo nuevamente.")
        except ValueError:
            print("Error: debe ingresar un número entero.")

def agregar_estudiante():
    
    clean_screen()
    
    ID = " "
    while consultar_existencia(ID,datos) != False or validar_identificacion(ID) != True:
        ID = identificacion("Ingrese documento de identidad del estudiante: ")
    datos[0].append(ID)

    clean_screen()
    listaNotas = []
    for i in listCourses:
        print("Nota: Si el estudiante cancelo la materia escribe -1 y si no matriculó un curso escribe -2:")
        print(f"Ingrese la nota del estudiante para la materia {i}: ")
        nota = solicitudNota()
        listaNotas.append(nota)
        clean_screen()
    datos[1].append(listaNotas)
    gestion_estudiantes()
    

def definir_datos(archivo):
    datos = [extraer_documentos(archivo), extraer_notas(archivo)]
    return datos


def consultar_existencia(string,diccionario):
    if string in diccionario:
        return True
    else:
        return False


def eliminar_estudiante():

    clean_screen()
    listVal = []
    opcionales = 1
    for i in datos[0]: 
        print(f"{opcionales}. {i}")
        listVal.append(str(opcionales))
        opcionales += 1

    listVal.append(str(opcionales))
    print(f"{opcionales}. Retroceder")
    opcionA = respuesta_numerica("Ingrese el usuario que desea eliminar: ",listVal)

    if opcionA in listVal[0:-1]:
        opcion = respuesta_numerica("¿Estas seguro? (1.) para continuar (2.) para retroceder: ",["1","2"])

        if opcion == "1":
            datos[0][int(opcionA)-1] = ""
            listAux = []
            i = 0
            while len(listAux) != len(datos[0])-1:
                if datos[0][i] != "":
                    listAux.append(datos[0][i])
                i += 1

            custom_clear(datos[0])
            for i in listAux:
                datos[0].append(i)

            custom_clear(datos[1][int(opcionA)-1])
            listAux = []
            i = 0
            for i in datos[1]:
                if len(i) != 0:
                    listAux.append(i)

            custom_clear(datos[1])
            for i in listAux:
                datos[1].append(i)
              

        elif opcion == "2":
            eliminar_estudiante()

    elif opcionA == listVal[-1]:
        gestion_estudiantes()


def agregar_curso():
    #Pendiente validar existencia
    codigo = listCourses[0]
    while consultar_existencia(codigo,listCourses) != False:
        codigo = input("Ingrese el código del curso: ")
        if consultar_existencia(codigo,listCourses) != False:
            print("Este código ya existe, ingrese uno nuevo")
    listCourses.append(codigo)

    i = 0
    clean_screen()
    print("Nota: Si el estudiante cancelo la materia escribe -1 y si no matriculó un curso escribe -2:")
    for j in datos[0]:
        print(f"Se le pedirá que ingrese la nota del curso {codigo} para el estudiante {datos[0][i]}")
        nota = solicitudNota()
        datos[1][i].append(nota)
        i += 1
    gestion_cursos()


def promedio_cursos():
    listaPromed = []

    cantI = len(listCourses)

    for i in range(0,cantI):
        listaAuxiliar = []
        for j in datos[1]:
            listaAuxiliar.append(j[i])
        listaPromed.append(round(sumExcepcion(listaAuxiliar)/lenExcepcion(listaAuxiliar),2))

    i = 0
    for j in listCourses:
        print(f"El curso {j} tuvo un promedio de {listaPromed[i]}")
        i += 1
    r = respuesta_numerica("Desea volver al menú analisis estadisticas (1.) o finalizar ejecución (2.): ",["1","2"])
    if r == "1":
        analisis_estadisticas()
    elif r == "2":
        finalizar_ejecucion()

def bubble_sort_usuarios(lista_numeros, lista_usuarios):
    n = len(lista_numeros)
    
    for i in range(n-1):
        for j in range(0, n-i-1):
            if lista_numeros[j] > lista_numeros[j+1]:
                # Intercambiar los números
                lista_numeros[j], lista_numeros[j+1] = lista_numeros[j+1], lista_numeros[j]
                # Intercambiar los usuarios correspondientes
                lista_usuarios[j], lista_usuarios[j+1] = lista_usuarios[j+1], lista_usuarios[j]


def seleccion_sort(lista,estudiantes):
    n = len(lista)
    
    for i in range(n):
        min_idx = i
        
        # Encuentra el índice del elemento más pequeño en el subarray sin ordenar
        for j in range(i+1, n):
            if lista[j] < lista[min_idx]:
                min_idx = j
        
        # Intercambia el elemento más pequeño encontrado con el primer elemento sin ordenar
        lista[i], lista[min_idx] = lista[min_idx], lista[i]
        estudiantes[i], estudiantes[min_idx] = estudiantes[min_idx], estudiantes[i]


def menor_nota_estudiante():
    clean_screen()
    listValidos = []

    indice = 1
    for i in datos[0]:
        print(f"{indice}. {i}")
        listValidos.append(str(indice))
        indice += 1

    opcion = int(respuesta_numerica("Seleccione el código del estudiante que desea analizar: ", listValidos))

    codigo_curso = ""

    posCodigo = 0
    notaMenor = 5.1
    cursosMatriculados = []
    for i in datos[1][opcion - 1]:
        if i == -1 or i == -2:
            posCodigo += 1
        elif i < notaMenor:
            cursosMatriculados.append(listCourses[posCodigo])
            notaMenor = i
            posCodigo += 1
     
    print(f"La menor nota fue {notaMenor} en la materia {listCourses[posCodigo - 1]}")
    r = respuesta_numerica("Desea volver al menú analisis estadisticas (1.) o finalizar ejecución (2.): ",["1","2"])
    if r == "1":
        analisis_estadisticas()
    elif r == "2":
        finalizar_ejecucion()

def promedio_estudiante(opcion):
    #Cada posición de la lista corresponde a un estudiante en el mismo orden
    listPromedio = []
    for j in datos[1]:
        listPromedio.append(round(sumExcepcion(j)/lenExcepcion(j),2))
    clean_screen()
    print("Lista de promedios: ")
    if opcion == 1:
        i = 0
        for j in datos[0]:
            print(f"El estudiante {j} tuvo un promedio de {listPromedio[i]}")
            i += 1
        r = respuesta_numerica("Desea volver al menú analisis estadisticas (1.) o finalizar ejecución (2.): ",["1","2"])
        if r == "1":
            analisis_estadisticas()
        elif r == "2":
            finalizar_ejecucion()
    elif opcion == 2:
        return listPromedio

def ordenar_promedio():
    clean_screen()
    global estudiantesOrdenados
    estudiantesOrdenados = datos[0]
    listPromedios = promedio_estudiante(2)
    bubble_sort_usuarios(listPromedios, estudiantesOrdenados)
    
    print("Estudiantes listados de mayor a menor: ")
    i = 1
    for elemento in listPromedios[::-1]:
        print(f"{i}. {estudiantesOrdenados[-i]} {elemento} y tiene la posición {cal_pos_group(estudiantesOrdenados[-i])} ")
        i += 1
    r = respuesta_numerica("Desea volver al menú analisis estadisticas (1.) o finalizar ejecución (2.): ",["1","2"])
    if r == "1":
        analisis_estadisticas()
    elif r == "2":
        finalizar_ejecucion()


def cal_pos_group(ID_estudiante):
    pos = 0
    for i in datos[0]:
        if i == ID_estudiante:
            return pos
        pos += 1



def tres_notas_mayores():
    clean_screen()
    listValidos = []
    indice = 1
    for i in listCourses:
        print(f"{indice}. {i}")
        listValidos.append(str(indice))
        indice += 1

    opcion = int(respuesta_numerica("Seleccione el código del curso que desea analizar: ", listValidos))

    listNotasAux = []
    listUsuariosAux = []

    es = 0
    for i in datos[1]:
        if i[opcion - 1] != -1 and i[opcion - 1] != -2:
            listNotasAux.append(i[opcion - 1])
            listUsuariosAux.append(datos[0][es])
        es += 1

    bubble_sort_usuarios(listNotasAux, listUsuariosAux)

    mejoresNotas = listNotasAux[-3:]
    for nota in mejoresNotas:
        for i in range(len(listNotasAux)):
            if listNotasAux[i] == nota:
                print(f"Documento de los estudiantes con las mejores notas : {listUsuariosAux[i]}")

    r = respuesta_numerica("Desea volver al menú analisis estadisticas (1.) o finalizar ejecución (2.): ",["1","2"])
    if r == "1":
        analisis_estadisticas()
    elif r == "2":
        finalizar_ejecucion()


def ordenamiento_cantidadEC():
    contCursos_General = []
    estudiantes = datos[0]

    for i in datos[1]:
        contCursos = 0
        for j in i:
            if j == -1 or j == -2:
                pass
            else:
                contCursos += 1
        contCursos_General.append(contCursos)

    seleccion_sort(contCursos_General,estudiantes)
    
    clean_screen()
    print("Estudiantes según la cantidad de materias cursadas de menor a mayor: ")
    for i in range(0, len(contCursos_General)):
        print(f"El estudiante {estudiantes[i]} está cursando {contCursos_General[i]} cursos")
    r = respuesta_numerica("Desea volver al menú analisis estadisticas (1.) o finalizar ejecución (2.): ",["1","2"])
    if r == "1":
        analisis_estadisticas()
    elif r == "2":
        finalizar_ejecucion()

def write_text(nombre_archivo):

    stringCursos = ""
    for i in listCourses:
        stringCursos += f"{i}, "

    stringCursos = stringCursos[:-2]

    with open(nombre_archivo, "w") as file:
        file.write("".join(stringCursos))
        file.write("\n\n")
        
        for i in datos[0]:
            file.write("".join(f"{i} "))
        file.write("\n\n")

        indice = 0
        for j in datos[1]:
            strAux = ""
            for k in j:
                strAux += f"{k} "
            file.write("".join(strAux))
            file.write("\n")

def finalizar_ejecucion():
    write_text("database.txt")
    print("Escribiendo información en la base de datos......")
    sleep(1)
    print("Información guardada con éxito")
    sleep(1)
    clean_screen()
    exit()



def copy_list(listaC):
    listW = []
    for i in listaC:
        listW.append(i)
    return listW


def ordenamiento_cancelaciones():
    clean_screen()
    cursos = copy_list(listCourses)
    contCancelaciones = list([0] * len(listCourses))

    for j in datos[1]:
        i = 0
        while i != len(j):
            if j[i] == -1:
                contCancelaciones[i] += 1
            i += 1

    seleccion_sort(contCancelaciones,cursos)
    clean_screen()
    print("Cursos ordenados por número de cancelaciones: ")
    for i in range(0, len(contCancelaciones)):
        print(f"El estudiante curso {cursos[i]} ha sido cancelado {contCancelaciones[i]} veces")

    r = respuesta_numerica("Desea volver al menú analisis estadisticas (1.) o finalizar ejecución (2.): ",["1","2"])
    if r == "1":
        analisis_estadisticas()
    elif r == "2":
        finalizar_ejecucion()

def menu_principal():

    clean_screen()
    print("Análisis de Estadísticas Estudiantes Medicina V1.0")
    print("---------------------------------------------------")
    lista_opciones = \
           "1. Gestión de Cursos\n" \
           "2. Gestión de Estudiantes\n" \
           "3. Estadisticas Generales\n" \
           "4. Finalizar Ejecución\n"\
           "---------------------------------------------------\n"
    print("Seleccione una de las opciones listadas:")
    modo = respuesta_numerica(lista_opciones,["1","2","3","4"])
    if modo == "1":
        gestion_cursos()
    elif modo == "2":
        gestion_estudiantes()
    elif modo == "3":
        analisis_estadisticas()
    elif modo == "4":
        finalizar_ejecucion()

def gestion_cursos():
    clean_screen()
    print("Gestion de Cursos")
    print("---------------------------------------------------")
    lista_opciones = \
           "1. Agregar Curso\n" \
           "2. Eliminar Curso\n" \
           "3. Retroceder\n" \
           "4. Finalizar Ejecución\n"\
           "---------------------------------------------------\n"
    print("Seleccione una de las opciones listadas:")
    modo = respuesta_numerica(lista_opciones,["1","2","3","4"])
    if modo == "1":
        agregar_curso()
    elif modo == "2":
        delete_course()
    elif modo == "3":
        menu_principal()
    elif modo == "4":
        finalizar_ejecucion()

def gestion_estudiantes():
    clean_screen()
    print("Gestion de Estudiantes")
    print("---------------------------------------------------")
    lista_opciones = \
           "1. Agregar Estudiante\n" \
           "2. Eliminar Estudiante\n" \
           "3. Retroceder\n" \
           "4. Finalizar Ejecución\n"\
           "---------------------------------------------------\n"
    print("Seleccione una de las opciones listadas:")
    modo = respuesta_numerica(lista_opciones,["1","2","3","4"])
    if modo == "1":
        agregar_estudiante()
    elif modo == "2":
        eliminar_estudiante()
    elif modo == "3":
        menu_principal()
    elif modo == "4":
        finalizar_ejecucion()

def analisis_estadisticas():
    clean_screen()
    print("Analisis de Estadisticas")
    print("---------------------------------------------------")
    lista_opciones = \
           "1. Promedio de Estudiantes\n" \
           "2. Promedio de Cursos\n" \
           "3. Tres Notas Mayores de un Curso\n" \
           "4. Menor nota de un estudiante\n"\
           "5. Ordenar Promedios Estudiantes\n"\
           "6. Ordenar Estudiantes por Cantidad de Cursos\n"\
           "7. Ordenar cursos según cancelaciones\n"\
           "8. Retroceder\n" \
           "9. Finalizar Ejecución\n"\
           "---------------------------------------------------\n"
    print("Seleccione una de las opciones listadas:")
    modo = respuesta_numerica(lista_opciones,["1","2","3","4","5","6","7","8","9"])
    if modo == "1":
        promedio_estudiante(1)
    elif modo == "2":
        promedio_cursos()
    elif modo == "3":
        tres_notas_mayores()
    elif modo == "4":
        menor_nota_estudiante()
    elif modo == "5":
        ordenar_promedio()
    elif modo == "6":
        ordenamiento_cantidadEC()
    elif modo == "7":
        ordenamiento_cancelaciones()
    elif modo == "8":
        menu_principal()
    elif modo == "9":
        finalizar_ejecucion()


