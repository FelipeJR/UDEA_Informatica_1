def validar_identificacion(documento):

    """
    Sinopsis:
        Función que permite validar si un documento de identidad tiene únicamente 10 digitos y son 
        digitos validos.

    Entradas y salidas:
        - documento: documento de identidad en str
        - returns: True o False dependiendo de si es valido
    """

    contDigitos = 0
    digitos = "0123456789"

    while contDigitos != 10:
        #contDigitos = 0
        for d in documento:
            if d in digitos:
                contDigitos += 1
            
            if contDigitos != 10 and len(documento) != 10 or d not in digitos:
                return False
    return True

def identificacion(texto):
    
    """
    Sinopsis:
        Función que le solicita al usuario que ingrese un documento de identidad y valida si cumple 
        con los requerimientos.


    Entradas y salidas:
        - None
        - returns: documento 
    """

    documento = " "
    while validar_identificacion(documento) != True:
        documento = input(texto)
        if validar_identificacion(documento) != True:
            print("El documento ya está registrado o es invalido")

    return documento

def respuesta_numerica(string,lista_validos):

    '''
        
        Sinopsis:
            Función que permite validar si el usuario ingresa un número de un conjunto especifico dado como argumento.
            
        Entradas y salidas:
            - digitos_validos_str: conjunto de números validos en str
            - string: texto que se mostrará por pantalla al ejecutar el input
            - returns: num valido
            
    '''
    num = ""
    while num not in lista_validos:
        num = input(string)
        if num not in lista_validos:
            print("╔════════════════════════╗")
            print("║¡La opción no es válida!║")
            print("║ Seleccione nuevamente  ║")
            print("╚════════════════════════╝")
    return num

def clean_screen():

    '''
    
    Sinopsis:
         Función que permite limpiar la pantalla ejecutando el comando correspondiente al SO donde se 
         está ejecutando el código para la limpieza de la terminal.
        
    Entradas y salidas:

        - returns: none, solo se limpia la pantalla
        
    '''

    import sys
    import os


    if sys.platform.startswith('win'):
        os.system('cls')
    else:
        print("\033c", end="")

def sleep(nSegundos):

    '''
    
    Sinopsis:
         Función permite simular el pausado de la ejecución del código
        
    Entradas y salidas:
        - nSegundos: segundos en los que se pausará la ejecución del codigo
        - returns: none
        
    '''

    from datetime import datetime
    tiempo_inicio = datetime.now()
    while (datetime.now() - tiempo_inicio).total_seconds() < nSegundos:
        pass

def custom_clear(lista):
    while len(lista) != 0:
        lista.pop()
    return lista

def custom_split(string, separadores):

    '''
        
        Sinopsis:
            Función que nos permite imitar la función nativa de python split, separando por un delimitador
            en una lista los elementos de un string. 
            
        Entradas y salidas:
            - string: string a evaluar.
            - separadores: delimitador a buscar.
            - returns: lista con sus elementos separados por un delimitador
            
    '''

    def search_position(linea, separadores):

        '''
        
        Sinopsis:
            Función que nos permite encontrar las ubicaciones exactas de los delimitadores en el string ingresado.
            
        Entradas y salidas:
            - linea: string a evaluar.
            - separadores: delimitador a buscar.
            - returns: posiciones del delimitador en el string
            
        '''

        pos = []
        for i in range(len(linea)):
            if linea[i] in separadores:
                pos.append(i)
        return pos


    pos = search_position(string, separadores) #Lista con las posiciones de los delimitadores en el string.

    list_elementos = []  #Aqui vamos a guardar los elementos separados 

    if len(pos) == 0:
        list_elementos.append(string)
        return list_elementos
    
    list_elementos.append(string[:pos[0]])

    for i in range(len(pos)):
        if i < len(pos) - 1:
            indice0 = pos[i] + 1
            indice1 = pos[i + 1]
            list_elementos.append(string[indice0:indice1])
        else:
            indice0 = pos[i] + 1
            list_elementos.append(string[indice0:])

    return list_elementos

def open_file(archivo):

    '''

    Sinopsis:
        Función permite cargar la información de un archivo de texto plano en memoria
        
    Entradas y salidas:
        - archivo: nombre del archivo que contiene la información que se cargará en memoria
        - return: información del archivo accesible por medio de indices
        
    '''
    
    with open(archivo) as file:
        lineas = file.readlines()
    return lineas


def posLineWhite(archivo):

    '''
    
    Sinopsis:
         funcion que permite calcular la posición de las lineas que está completamente en blanco de un archivo
        
    Entradas y salidas:
        - archivo: nombre del archivo que contiene la información que se cargará en memoria
        - return: lista con posiciones en blanco
        
    '''

    lectura = open_file(archivo) 
    posLineWhite = []
    pos = 0

    for linea in lectura:
        linea_limpia = linea.rstrip()
        if len(linea_limpia) == 0:
            posLineWhite.append(pos)
        pos += 1

    return posLineWhite


def sumExcepcion(lista):
    suma = 0
    for i in lista:
        if i == -1 or i == -2:
            pass
        else:
            suma += float(i)
    return suma

def lenExcepcion(lista):
    cont = 0
    for i in lista:
        if i == -1 or i == -2:
            pass
        else:
            cont += 1
    return cont

