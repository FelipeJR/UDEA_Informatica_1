from funcionalidades import *
from modulos_generales import *


def initEstructuraX():
    global listCourses
    listCourses = extraer_cursos("database.txt")
    global datos
    datos = definir_datos("database.txt")


def eliminarUsuario_Optifine():
    cont = 0
    clean_screen()
    listval = []
    
    i = 0
    if len(datos[0]) % 2 == 0:
        while i < len(datos[0]):
            print(f"{i+1}. {datos[0][i]}")
            listval.append(str(i+1))
            print(f"{i+2}. {datos[0][i+1]}")
            listval.append(str(i+2))
            i += 2
            cont += 1
    else: 
        while i < len(datos[0])-1:
            print(f"{i+1}. {datos[0][i]}")
            listval.append(str(i+1))
            print(f"{i+2}. {datos[0][i+1]}")
            listval.append(str(i+2))
            i += 2
            cont += 1
        print(f"{i+1}. {datos[0][i]}")
        listval.append(str(i+1))
        i += 1

    listval.append(str(i+1))
    print(f"{i+1}. Retroceder")

    opcionA = respuesta_numerica("Ingrese el usuario que desea eliminar: ", listval)

    if opcionA in listval[0:-1]:
        opcion = respuesta_numerica("¿Estás seguro? (1.) para continuar (2.) para retroceder: ", ["1", "2"])

        if opcion == "1":
            datos[0][int(opcionA) - 1] = ""
            datos[0] = [dato for dato in datos[0] if dato != ""]
            cont += len(datos[0])

            custom_clear(datos[1][int(opcionA)-1])
            datos[1] = [dato for dato in datos[1] if len(dato) != 0]
            cont += len(datos[1])

        elif opcion == "2":
            gestion_estudiantes()

    elif opcionA == listval[-1]:
        gestion_estudiantes()
        
    print(f"Ocurrieron {cont} iteraciones")









def ordenamiento_cancelaciones_optifine():
    
    def quicksort(arr, estudiantes):
        if len(arr) <= 1:
            return arr, estudiantes
        else:
            pivote = arr[-1]
            menores, iguales, mayores = [], [], []
            for i in range(len(arr)):
                if arr[i] < pivote:
                    menores.append(arr[i])
                    estudiantes[i], estudiantes[len(menores) - 1] = estudiantes[len(menores) - 1], estudiantes[i]
                elif arr[i] == pivote:
                    iguales.append(arr[i])
                else:
                    mayores.append(arr[i])
                    estudiantes[i], estudiantes[len(menores) + len(iguales) + len(mayores) - 1] = estudiantes[len(menores) + len(iguales) + len(mayores) - 1], estudiantes[i]
            
            menores, estudiantes[:len(menores)] = quicksort(menores, estudiantes[:len(menores)])
            mayores, estudiantes[len(menores) + len(iguales):] = quicksort(mayores, estudiantes[len(menores) + len(iguales):])
            
            return menores + iguales + mayores, estudiantes
        
    clean_screen()
    cursos = copy_list(listCourses)
    cancelaciones = [0] * len(listCourses)

    for j in datos[1]:
        for i in range(len(j)):
            if j[i] == -1:
                cancelaciones[i] += 1

    cancelaciones, cursos = quicksort(cancelaciones, cursos)
    
    clean_screen()
    print("Cursos ordenados por número de cancelaciones: ")
    for i in range(len(cancelaciones)):
        print(f"El estudiante del curso {cursos[i]} ha sido cancelado {cancelaciones[i]} veces")

    r = respuesta_numerica("Desea volver al menú de análisis estadísticas (1) o finalizar ejecución (2): ", ["1", "2"])
    if r == "1":
        analisis_estadisticas()
    elif r == "2":
        finalizar_ejecucion()