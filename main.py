import math #Importación de librería math
import matplotlib.pyplot as plt #Importación de librería matplotlib para la realización de gráficas
from funciones import * #Importación de funciones


#Se llama la función para inicializar las variables
init_var()

#Se define un rango de 0 a 720
theta = range(0, 721, 1)  

#Se usa una estrategia de compresión de lista para almacenar los valores de la posición por cada angulo
position_values = [calculate_position(math.radians(t)) for t in theta] 

#Se usa una estrategia de compresión de lista para almacenar los valores del cambio de la velocidad
velocity_values = [calculate_velocity(math.radians(t), 1000 / 60 * 2 * math.pi) for t in theta] 

#Se usa una estrategia de compresión de lista para almacenar el cambio de la aceleración
acceleration_values = [calculate_acceleration(math.radians(t), 1000 / 60 * 2 * math.pi) for t in theta] 

#Se usa una estrategia de compresión de lista para almacenar los valores del volumen
volume_values = [calculate_volume(math.radians(t)) for t in theta] 

#Se usa una estrategia de compresión de lista para almacenar el cambio en los volumenes
delta_volume_values = [volume_values[i] - volume_values[i-1] if i > 0 else 0 for i in range(len(volume_values))]

#Se usa una estrategia de compresión de lista para almacenar los valores del cambio de la presión respecto al ángulo, pues el ángulo hace cambiar el resto de variables 
presion_values = [calculate_pressure(theta[i],delta_volume_values[i]) for i in range(0,len(theta))] 


#Se inicializa las 5 gráficas
fig, axs = plt.subplots(5, figsize=(10, 15)) 

# Ajuste de márgenes
plt.subplots_adjust(hspace=0.8)   


#Gráfica 1
axs[0].plot(theta, position_values) #Asignación de las variables a comparar en la tabla
axs[0].set_title('Cambio en Posición del Pistón') # Asignación de titulo
axs[0].set_ylabel('s (cm)')

#Gráfica 2
axs[1].plot(theta, velocity_values) #Asignación de las variables a comparar en la tabla
axs[1].set_title('Velocidad del Pistón') # Asignación de titulo
axs[1].set_ylabel('v (cm/s)')

#Gráfica 3
axs[2].plot(theta, acceleration_values) #Asignación de las variables a comparar en la tabla
axs[2].set_title('Aceleración del Pistón') # Asignación de titulo
axs[2].set_ylabel('a (cm/s^2)')

#Gráfica 4
axs[3].plot(theta, delta_volume_values) #Asignación de las variables a comparar en la tabla
axs[3].set_title('Volumen en Cámara de Combustión') # Asignación de titulo
axs[3].set_ylabel('ΔV (cm^3)')

#Gráfica 5
axs[4].plot(theta, presion_values) #Asignación de las variables a comparar en la tabla
axs[4].set_title('Cambio de presión con respecto al angulo') # Asignación de titulo
axs[4].set_xlabel('ΔV (m^3)')
axs[4].set_ylabel('ΔP (Theta)')  

#Se muestran las gráficas
plt.show()
